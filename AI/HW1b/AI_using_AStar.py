# CSCI 561, Fall 2018, Homework 1b
# Author: Leland Launer
# This program will contain the agent program for offline search agent
# trying to determine the optimal placement of police officers
# on a grid of city blocks.

import numpy as np
import itertools
import pdb
import Queue
import logging
import pprint
pp=pprint.PrettyPrinter() #this will be nice for displaying the grid
logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s - %(levelname)s- %(message)s')
logging.disable(logging.CRITICAL)


'''
For those interested, let me explain the problem.
We have a given number of police officers that we have
to place on a n x n grid that abstractly represent city blocks.

There are two considerations that guide this process:
1) There are "activity points" that represent scooter traffic activity that
the officers should be monitoring. Thus, the placement of the officers
should maximize these activity points.
2) No officers can be placed on the same street. That means, for this grid,
you cannot place an officer in the same row, column, or
diagonal as another officer.
(Thus, this problem is a variant on the n-queens problem)

An additional requirement: you must place all police officers on the grid.

Also, as a kindness, n was limited to 15 and below (n<=15).

Finally, all irregular inputs were forbidden, so I do not account for them in the program.

That is the problem definition. What follows is my reasoning towards a solution.

WHY UNINFORMED SEARCH IS INSUFFICIENT

So in approaching this problem, I knew that a comprehensive, uninformed search
(e.g. depth- or breadh-first search) would take far too long to compute an answer
for some cases allowed by n=15. Let's look at this formulation and see its
worst-case performance.

For example, let's assume that n=15 and the number of police officers p=6.
If we were to build up a solution incrementally, by assigning one officer
to the grid at a time and checking to see if the assignment is valid
after it has been fully constructed, then we would have 15^2=225 tiles
to choose from for the first officer, 224 tiles for the second officer,
223 for the third, etc.

So ultimately, we have 225*224*223*222*221*220= 1.2 * 10^14 different
ways to assign these six officers to the grid. This is far too many states
to check. Even if we could check 1 million of them per second, we would still
have to spend 1.2 * 10^8 seconds, or about 90 years, to get the right answer.
Formally, the worst case performance is O((n^2)!/(n^2-p)!), which is ghastly

Now, you can greatly improve the performance by limiting the assignment of one officer
to a given row, so as to enforce part of the constraints immediately. You can also improve performance
by keeping an updated map of the grid and marking "invalid" tiles that
are in the same row, column, or diagonal as an already assigned officer.
Though not taking years to complete, the searches could still stall for several
minutes. There needed to be some information that the search
could use to implicitly prune different branches of the search tree.

INVERTING A*

A* can do this kind of pruning, given the right information.
A* search usually deals with minimizing a function that deals
with current and future costs. However, since I wanted to maximize
activity points, I implemented A* so that it maximized a function
that deals with current and future benefits
(i.e. the activity points it already has from assigned officers and the maximum
points it could achieve in tiles that are still valid/not invalid).

With A*, the number of assignments that need to be checked for
a given grid and number of officers shrinks from trillions to
a few thousand, and makes the search doable in a few seconds
for all cases.

HOW TO OPERATE THE PROGRAM

All you need is a text file named "input?.txt," where the
number is a variable that you can manually update below.

The file needs the following format:
first line should have n, the dimensions of the grid
second line should have p, the number of officers
third line should have s, the number of scooters or other vehicles
the remaining s*12 lines should each contain a coordinate
that specifies where the vehicles are in the grid
at a specific time, which correlates to an activity point

The program will create a text file called "output.txt"
that has the answer to the question, which is the maximum
activity points that can be achieved given the circumstances

The program also prints on the terminal a view of the grid
with the activity points per tile, along with a solution
grid and the maximum activity points of the solution

Several input files are provided, so feel free to use them

'''



inputFileNumber="47"


#####PART 0 - CLASSES, FUNCTIONS, CONSTANTS

INVALID_POS=0
VALID_POS=1
OFFICER_POS=2



#NOTE: "position" is often used as the x-coordinate on the grids
# whereas "street" is often used as the y-coordinate on the grids

class streetComboNode:
    def __init__(self, activityGrid, officerGrid, gFromPreviousNode, xCoord, yCoord, newUnassignedOfficers):
        self.officerGrid=officerGrid.copy()
        self.unassignedOfficers=newUnassignedOfficers
        self.yCoord=yCoord
        self.xCoord=xCoord
        if (xCoord is None):
            self.g=gFromPreviousNode
        else:
            self.updateOffGrid(yCoord, xCoord) #update the offGrid with the new officer
            self.g=self.sumPathBenefits(activityGrid, gFromPreviousNode, xCoord, yCoord) # g will be current benefits (i.e. activity points) accrued along this path
        self.h=self.sumFutureBenefits(activityGrid) # h will be the estimated future benefits in getting to a valid configuration
        self.f=self.g+ self.h# f will be the estimated maximum benefit (i.e. activity score) on this path towards a valid configuration
        #self.parentNode=parentNode #this is probably unnecessary given that we do not need to return a path
    def __cmp__(self, other):
        return -cmp(self.f, other.f) #NOTE: the minus sign here is so that the priority queue is ordered from highest f to lowest f
    def updateOffGrid(self, street, position):
        #self.officerGrid[street][position]=INVALID_POS #since we're placing an officer here, no other officer can be placed here
        for sameColumn in self.officerGrid: #no other officer can be placed in the same column
            sameColumn[position]=INVALID_POS
        for i in range(self.officerGrid[street].shape[0]):
            self.officerGrid[street][i]=INVALID_POS #no other officer can be placed in the same row
        self.updateDiagonals(street, position) #no other officer can be placed in the diagonal(s)
        self.officerGrid[street][position]=OFFICER_POS
    def updateDiagonals(self, street, position):
        dimension=self.officerGrid.shape[0]
        rightDiagonalStreet=street
        rightDiagonalPosition=position
        while (rightDiagonalStreet>=0 and rightDiagonalStreet<dimension
               and rightDiagonalPosition>=0 and rightDiagonalPosition<dimension):
            self.officerGrid[rightDiagonalStreet][rightDiagonalPosition]=INVALID_POS;
            rightDiagonalStreet+=1
            rightDiagonalPosition+=1
        leftDiagonalStreet=street
        leftDiagonalPosition=position
        while (leftDiagonalStreet>=0 and leftDiagonalStreet<dimension
               and leftDiagonalPosition>=0 and leftDiagonalPosition<dimension):
            self.officerGrid[leftDiagonalStreet][leftDiagonalPosition]=INVALID_POS;
            leftDiagonalStreet+=1
            leftDiagonalPosition-=1
    def calculateActivity(self, activityGrid):
        maxActivity=0
        n=self.officerGrid.shape[0]
        for i in range(n):
            for j in range(n):
                if self.officerGrid[i][j]==OFFICER_POS:
                    maxActivity+=activityGrid[i][j]
        logging.debug("Possible activity score: "+str(maxActivity))
        return maxActivity
    """PSEUDO/PLANNING - need to have a function that calculates/adds to g(n)"""
    def sumPathBenefits(self, activityGrid, gFromPreviousNode, xCoord, yCoord):
        if xCoord!=None:
            return gFromPreviousNode+activityGrid[yCoord][xCoord]
        else:
            return gFromPreviousNode
    """PSEUDO/PLANNING - need to have a function that calculates h(n) """
    def sumFutureBenefits(self, activityGrid):
        """PSEUDO/PLANNING - create new 2D array by checking the activityGrid against
        the officerGrid"""
        maxValidGrid=activityGrid.copy()
        for yCoord in range(maxValidGrid.shape[0]):
            for xCoord in range(maxValidGrid.shape[0]):
                if self.officerGrid[yCoord][xCoord]==INVALID_POS:
                    maxValidGrid[yCoord][xCoord]=0;
                    
        """PSEUDO/PLANNING - find the tile of the maximum activity points in the LOWER rows/streets
        in this new grid"""
        maxForAGivenRow=[]
        if self.yCoord==-1: #initial node
            depth=0
        else:
            depth=self.yCoord+1 #needs to be yCoord+1, as we (may) have just placed an officer at yCoord
        for row in maxValidGrid[depth:]:
            maxForAGivenRow.append(np.amax(row))

        """PSEUDO/PLANNING - Now sort this list of maximum values and take the top values for
        however many officers are remaining"""
        logging.debug("Here is the max for the positions available to the newly generated node:\n "
                      +str(maxForAGivenRow))
        maxForAGivenRow.sort(reverse=True)
        logging.debug("Again, but sorted:\n"+str(maxForAGivenRow))

        h=0
        for i in range(self.unassignedOfficers):
            h+=maxForAGivenRow[i]
        return h

""" PSEUDO/PLANNING - implement A* search with path benefits instead of path costs
    activityGrid: the nxn grid that indicates where the scooter activity points are
    This function returns the f score of the first goal node that it expands"""
def aStar(n,p,s, activityGrid):
    #PSEUDO/PLANNING - create local variables like frontier priority queue, action list
    frontier=Queue.PriorityQueue()
    actionList=[xCoord for xCoord in range(n)]
    actionList.append(None) #this is the action that signifies not placing an officer in that row of the grid
    validPositions=np.array([VALID_POS for i in range(n)])
    initialOfficerGrid=np.array([validPositions.copy()for i in range(n)], dtype=int)
    
    #PSEUDO/PLANNING - create initial node with empty officerGrid
    initialNode=streetComboNode(activityGrid, initialOfficerGrid, 0, None, -1, p)
    #PSEUDO/PLANNING - add initial node to frontier
    frontier.put(initialNode)
    nodesExpanded=0
    nodesGenerated=0
    #PSEUDO/PLANNING - main loop while frontier is not empty
    while(not frontier.empty()):
    #PSEUDO/PLANNING - pop off node from the frontier
        currentNode=frontier.get()
        nodesExpanded+=1
    #debug
        logging.debug("\nThe h of the current node is "+str(currentNode.h)+
        "\nThe g of the current node is "+str(currentNode.g)+
        "\nThe f of the current node is "+str(currentNode.f))
        logging.debug("Number of nodes generated thus far: "+str(nodesGenerated))
        logging.debug("Number of nodes expanded thus far: "+str(nodesExpanded))
        logging.debug("The officerGrid of the currentNode is below: \n"+str(currentNode.officerGrid))
    #PSEUDO/PLANNING - perform goal test
        if(currentNode.unassignedOfficers==0):
            #debug - print out the grid with the assigned officers
            pp.pprint(currentNode.officerGrid)
            #PSEUDO/PLANNING - return f
            return currentNode.f
    #PSEUDO/PLANNING - expand the node by generating child nodes for each action, add each to the frontier
        for xCoord in actionList:
            if(xCoord is not None):
                if(currentNode.officerGrid[currentNode.yCoord+1][xCoord]==VALID_POS):#need to check if action/xCoord is a valid, non-conflicting placement of the next officer
                    newNode=streetComboNode(activityGrid, currentNode.officerGrid, currentNode.g, xCoord, currentNode.yCoord+1, currentNode.unassignedOfficers-1)
                    frontier.put(newNode)
                    nodesGenerated+=1
                else:
                    #leave blank, since a node should not be created on an invalid position
                    continue
            elif (n-(currentNode.yCoord+1)-currentNode.unassignedOfficers>0): #need to make sure there are still enough empty rows ahead for the unassigned cops before allowing nonassignment in the newNode
                newNode=streetComboNode(activityGrid, currentNode.officerGrid, currentNode.g, xCoord, currentNode.yCoord+1, currentNode.unassignedOfficers)
                frontier.put(newNode)
                nodesGenerated+=1
            else:
                #leave blank, since no new node should be created here
                continue

#####PART 1 - SETUP
if __name__=="__main__":
    #PSEUDO/PLANNING - create file object for input.txt
    inputReader=open("input%s.txt"%inputFileNumber, "r")
    #PSEUDO/PLANNING - read input, translate into internal data structures
    #like "activity points" grid
    n=int(inputReader.readline()) #n represents the length of a side of the square grid
    p=int(inputReader.readline()) #p represents the number of police officers
    s=int(inputReader.readline()) #s represents the number of scooters

    #create the "activity points" grid
    activityStreets=np.zeros(n, dtype=int)
    activityGrid=np.array([activityStreets.copy() for i in range(n)], dtype=int)
    #add to the activity points grid
    for line in inputReader:
        activityCoords=line.strip().split(",")
        activityGrid[int(activityCoords[1])][int(activityCoords[0])]+=1

    #DEBUGGING - print out "actitivy points" grid
    pp.pprint(activityGrid)
    print("Above is the activity grid.")

    #PSEUDO/PLANNING - close input file object
    inputReader.close()


    #####PART 2 - SEARCH
    maxActivity=aStar(n, p, s, activityGrid)
    print("Above is the solution grid, where "+str(OFFICER_POS)+" represents the position of an assigned officer.")
    print("The maximum activity points possible is: "+str(maxActivity))
    
    """ PSEUDO/PLANNING - finally, write the max activity score to a file named output.txt """
    outputFile=open("output.txt","w")
    outputFile.write(str(maxActivity))
    outputFile.close()
    



