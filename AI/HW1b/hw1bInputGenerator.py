#CSCI 561, Fall 2018, Homework 1b
#Author: Leland Launer
# This program will generate input files for Homework 1b

import random, string

""" PSEUDO/PLANNING - assign n=15 and p=7 for the worst case """
n=15
p=7
s=random.randint(10,15)
scooterLocations=[]
scooterString=""

""" PSEUDO/PLANNING - create a file object """
inputFile=open("input4.txt", "w")

""" PSEUDO/PLANNING - write n, p, and s """
inputFile.write(str(n)+"\n"+str(p)+"\n"+str(s)+"\n")

""" PSEUDO/PLANNING - generate scooter locations """
for scooter in range(s):
    scooterLocation=[random.randint(0,n-1), random.randint(0,n-1)]
    scooterLocations.append(scooterLocation)

for timestep in range(12):
    newScooterLocations=[]
    for scooterLocation in scooterLocations:
        scooterString+=",".join(str(e) for e in scooterLocation)+"\n"
        #determine which movements are viable
        validMoves=[]
        for scooterIndex in [0,1]:
            scooterCoord=scooterLocation[scooterIndex]
            for move in [-1,1]:
                if (scooterCoord+move)>=0 and (scooterCoord+move<=(n-1)):
                    validMove=scooterLocation[:]
                    validMove[scooterIndex]+=move
                    validMoves.append(validMove)
        newScooterLocations.append(random.choice(validMoves))
    scooterLocations=newScooterLocations

scooterString=string.rstrip(scooterString)
inputFile.write(scooterString)
inputFile.close()



