# CSCI 561, Fall 2018, Homework 2
# Author: Leland Launer
# This program will contain the agent program for an agent
# that seeks to maximize its efficiency in allocating resources
# to a homeless population in Los Angeles

import pdb
import logging
import copy
import numpy as np
#import pprint
#pp=pprint.PrettyPrinter()
logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s - %(levelname)s- %(message)s')
logging.disable(logging.CRITICAL)

#####PART 0 - CLASSES, FUNCTIONS, CONSTANTS
INPUT_FILE_NUM="1"
SPLA_NAME="SPLA"
LAHSA_NAME="LAHSA"
SPLA_EVAL_INDEX=SPLA_NAME
LAHSA_EVAL_INDEX=LAHSA_NAME
DAYS_OF_WEEK=7


'''PSEUDO/PLANNING - create an applicant class that will facilitate
interactions with applicants, like checking if they qualify under
a given agency's guidelines'''

class Applicant:
    def __init__(self, appString):
        self.appString=appString
        self.id=appString[0:5] #still a string
        self.gender=appString[5]
        self.age=int(appString[6:9])
        if(appString[9]=='Y'):
            self.pets=True
        else:
            self.pets=False
        if(appString[10]=='Y'):
            self.med=True
        else:
            self.med=False
        if(appString[11]=='Y'):
            self.car=True
        else:
            self.car=False
        if(appString[12]=='Y'):
            self.license=True
        else:
            self.license=False
        self.days=np.array(list(appString[13:20]), dtype=int)
    def __str__(self):
        return """Applicant string: %s\n
                Applicant ID: %s\n
                Gender: %s\n
                Age: %d\n
                Pets: %s\n
                Medical conditions: %s\n
                Car: %s\n
                Driver's license: %s\n
                Days vector: %s\n
                """%(self.appString, self.id, self.gender, self.age,
                     self.pets, self.med, self.car, self.license, self.days)
    '''
    This function will determine whether an applicant
    qualifies for a given agency
    agency: a string of the agency's name
    returns True/False
    '''
    def qualifiesFor(self, agency):
        assert agency in ['SPLA', 'LAHSA']
        if(agency=='SPLA'):
            return self.car and self.license and (not self.med)
        elif(agency=='LAHSA'):
            return self.gender=='F' and (self.age>17) and (not self.pets)
        else:
            raise Exception
        
    '''PSEUDO/PLANNING - create function that calculates efficiency for a given applicant'''
    def findEfficiency(self):
        return np.sum(self.days)

'''PSEUDO/PLANNING: create to represent the nodes is the search tree'''
class GameNode:
    def __init__(self, choosingAgency, currentApplicant, waitingAgency):
        self.choosingAgency=choosingAgency.copy()
        self.choosingAgency.chooseApplicant(currentApplicant)
        self.waitingAgency=waitingAgency.copy()
        self.waitingAgency.removeApplicantFromAvailable(currentApplicant)
    def evaluateState(self):
        #PSEUDO/PLANNING - evaluate for each of the agencies, then combine scores into canonical form
        evalScores={}
        evalScores[self.choosingAgency.name]=self.choosingAgency.evaluateForAgency()
        evalScores[self.waitingAgency.name]=self.waitingAgency.evaluateForAgency()
        return evalScores
        

class Agency:
    def __init__(self, name, chosen, available, spaces):
        self.name=name
        self.chosen=chosen #list of chosen applicants
        self.available=available #dictionary of available applicants, where applicant.id is the key
        self.spaces=spaces #array of spaces available per day, where Monday is the first position

        '''
        this function is meant to just create shallow copies of the key attributes.
        the applicant objects that held inside do NOT need to be copied
        '''
    def copy(self):
        return Agency(self.name, copy.copy(self.chosen), self.available.copy(), np.copy(self.spaces))
    def chooseApplicant(self, applicant):
        #first, add this applicant to the chosen group
        self.chosen.append(applicant)
        #second, delete the applicant from available
        del self.available[applicant.id]
        #third, update the available group, in case this applicant gets
        #the last bed/parking space for a given day, thus barring other applicants
        #from requesting the same day
        self.updateAvailable(applicant)
        #fourth, remove the spaces that this applicant will be using
        self.spaces-=applicant.days
 
    def updateAvailable(self, applicant):
        #cycle through the spaces and days arrays, checking if any of the spaces for a given day
        #have been completely allocated
        daysRecentlyCompletelyAllocated=[]
        for i in range(7):
            if(self.spaces[i]-applicant.days[i])==0:
                daysRecentlyCompletelyAllocated.append(i)
        #now "iterate" over the dictionary of available applicants and remove those
        #who are requesting a day that the agency can no longer accomodate
        for ID in self.available.keys():
            currentApplicant=self.available[ID]
            for day in daysRecentlyCompletelyAllocated:
                if currentApplicant.days[day]>0:
                    del currentApplicant
                    break
        '''
        if another agency chooses an applicant,
        then this agency must remove it from its available candidates
        applicant: the Applicant instance chosen by the other agency
        does not return anything
        '''
    def removeApplicantFromAvailable(self, applicant):
        try:
            del self.available[applicant.id]
        except KeyError:
            pass
        except Exception as e:
            raise e

    def evaluateForAgency(self):
        #keep it basic for the time being, just return the efficiencies of each agency
        totalEfficiency=0
        for chosenApplicant in self.chosen:
            totalEfficiency+=chosenApplicant.findEfficiency()
        return totalEfficiency


'''
Here is the search algorithm, modeled after game-playing where
two competitive agents take turns trying to maximize their "score"
'''
def startOfGame(SPLA, LAHSA):
    #PSEUDO/PLANNING - "iterate" over SPLA's dictionary of available applicants,
    #keeping track of the max score that has been returned and
    #the applicant that led to that max score
    maxScores=None
    maxApplicant=None
    for ID in SPLA.available.keys():
        nextChosenApplicant=SPLA.available[ID]
        nextGameNode=GameNode(SPLA, nextChosenApplicant, LAHSA)
        theScores=playGame(nextGameNode)
        logging.debug("For applicant "+nextChosenApplicant.id+", the scores are "+str(theScores))
        if(maxScores==None):
            maxScores=theScores
        if(theScores[SPLA_EVAL_INDEX]>maxScores[SPLA_EVAL_INDEX]):
            maxApplicant=nextChosenApplicant
            maxScores=theScores
    return maxApplicant

def playGame(currentGameNode):
    #PSEUDO/PLANNING - base case - return if max efficiency has been reached by SPLA. THIS IS REDUNDANT
    #AND IS MERELY A SPECIAL CASE OF THE BASE CASE WHERE THERE ARE NO MORE AVAILABLE APPLICANTS
    #PSEUDO/PLANNING - recursive case - have the waiting agency in the currentGameNode
    #"iterate" over its dictionary of available applicants
    nextChoosingAgency=None
    nextWaitingAgency=None
    maxScores=None
    if(len(currentGameNode.waitingAgency.available)>0):
        nextChoosingAgency=currentGameNode.waitingAgency
        nextWaitingAgency=currentGameNode.choosingAgency
    elif(currentGameNode.choosingAgency.name==SPLA_NAME and len(currentGameNode.choosingAgency.available)>0):
        nextChoosingAgency=currentGameNode.choosingAgency
        nextWaitingAgency=currentGameNode.waitingAgency
    else:
        #base case: no more available applicants for either agency
        #thus, call the evaluationFunction
        return currentGameNode.evaluateState()
    for ID in nextChoosingAgency.available.keys():
        nextChosenApplicant=nextChoosingAgency.available[ID]
        nextGameNode=GameNode(nextChoosingAgency, nextChosenApplicant, nextWaitingAgency)
        theScores=playGame(nextGameNode)
        if(maxScores==None):
            maxScores=theScores
        if(nextChoosingAgency.name==SPLA_NAME and theScores[SPLA_EVAL_INDEX]>maxScores[SPLA_EVAL_INDEX]):
            maxScores=theScores
        if(nextChoosingAgency.name==LAHSA_NAME and theScores[LAHSA_EVAL_INDEX]>maxScores[LAHSA_EVAL_INDEX]):
            maxScores=theScores
    return maxScores #base case - after evaluating all options from this node, return maxScores
        
    ##PSEUDO/PLANNING - for each available applicant, form a newGameNode where that applicant
    #has just been chosen by the waiting agency of the currentGameNode
    #PSEUDO/PLANNING - base case - no more available applicants, call the evaluation function
    
        
        
    
#####PART 1 - SETUP
if __name__=="__main__":
    applicantStrings=[]
    chosenLAHSAStrings=[]
    chosenSPLAStrings=[]
    applicants={}
    chosenLAHSA=[]
    chosenSPLA=[]
    availableLAHSA={}
    availableSPLA={}
    #PSEUDO/PLANNING - create file object for input.txt
    inputReader=open("input%s.txt"%INPUT_FILE_NUM, "r")
    b=int(inputReader.readline()) #number of beds in the shelter of LAHSA, the competing agent to SPLA
    #Additional note on b: b<=40
    p=int(inputReader.readline()) #number of spaces in the parking lot of SPLA, which our agent represents
    L=int(inputReader.readline()) #number of applicants chosen by LAHSA so far
    for uselessIndex in range(L): #obtain list of IDs for the applicants chosen by LAHSA
        chosenLAHSAStrings.append(inputReader.readline().rstrip())
    S=int(inputReader.readline()) #number of applicants chosen by SPLA so far
    for uselessIndex in range(S): #obtain list of IDs for applicants chosen by SPLA
        chosenSPLAStrings.append(inputReader.readline().rstrip())
    A=int(inputReader.readline()) #total number of applicants
    for uselessIndex in range(A):
        applicantStrings.append(inputReader.readline().rstrip())
    inputReader.close()

    '''Input file has been read. Now, must preprocess data.'''
    #PSEUDO/PLANNING - create a hash table for the general applicant pool
    for applicantString in applicantStrings:
        newApplicant=Applicant(applicantString)
        applicants[newApplicant.id]=newApplicant

    #PSEUDO/PLANNING - find the applicants already chosen by SPLA
    for chosenID in chosenSPLAStrings:
        applicantSPLA=applicants[chosenID]
        chosenSPLA.append(applicantSPLA)

    #PSEUDO/PLANNING - find the applicants already chosen by LAHSA, just as above
    for chosenID in chosenLAHSAStrings:
        applicantLAHSA=applicants[chosenID]
        chosenLAHSA.append(applicantLAHSA)

    logging.debug("Initially, here are the applicants chosen by LAHSA:\n")
    for applicantLAHSA in chosenLAHSA:
        logging.debug(str(applicantLAHSA)+"\n")

    logging.debug("Initially, here are the applicants chosen by SPLA:\n")
    for applicantSPLA in chosenSPLA:
        logging.debug(str(applicantSPLA)+"\n")


    #PSEUDO/PLANNING - from the general pool, form the pools available to
    #both of the agencies
    for applicantID in applicants.keys():
        applicant=applicants[applicantID]
        if(applicant in chosenSPLA or applicant in chosenLAHSA):
            continue #if already chosen, don't put in either available pool
        if(applicant.qualifiesFor(SPLA_NAME)):
            availableSPLA[applicantID]=applicant
        if(applicant.qualifiesFor(LAHSA_NAME)):
           availableLAHSA[applicantID]=applicant

    logging.debug("Initially, here are the applicants who are available and qualify for SPLA:\n")
    for ID in availableSPLA.keys():
        logging.debug(str(availableSPLA[ID])+"\n")

    logging.debug("Initially, here are the applicants who are available and qualify for LAHSA:\n")
    for ID in availableLAHSA.keys():
        logging.debug(str(availableLAHSA[ID])+"\n")

    #create arrays for the agencies that represent the resources they have for a given day of the week
    spacesSPLA=np.empty(DAYS_OF_WEEK, dtype=int)
    spacesSPLA.fill(p)
    spacesLAHSA=np.empty(DAYS_OF_WEEK, dtype=int)
    spacesLAHSA.fill(b)

    '''Preprocessing is complete. Now run the search algorithm'''
    SPLA=Agency(SPLA_NAME, chosenSPLA, availableSPLA, spacesSPLA)
    LAHSA=Agency(LAHSA_NAME, chosenLAHSA, availableLAHSA, spacesLAHSA)
    optimalFirstApplicant=startOfGame(SPLA, LAHSA)
    logging.debug("The applicant that SPLA should accept first is : "+optimalFirstApplicant.id)


    '''Finally, write the output to a file'''
    outputWriter=open("output.txt","w")
    outputWriter.write(optimalFirstApplicant.id)
    outputWriter.close()


    
           
        
        
    
    
        
