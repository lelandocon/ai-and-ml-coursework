# CSCI 561, Fall 2018, Homework 3
# Author: Leland Launer
# This program will help create good test cases for
# the homework 3 agent

import numpy as np
import math
import random
import logging
logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s - %(levelname)s- %(message)s')

OBSTACLE_ASCII="X"
o=OBSTACLE_ASCII
START_ASCII="S"
s=START_ASCII
BLANK_ASCII="0"
b=BLANK_ASCII
END_ASCII="E"
e=END_ASCII

sampleASCIIGrid=[[b,b,b,b,b,b,b,b,b,b],
                 [b,b,b,b,b,b,b,b,b,b],
                 [b,b,b,b,b,b,b,b,b,b],
                 [b,b,b,b,e,b,b,b,b,b],
                 [b,b,b,b,b,b,b,b,b,b],
                 [b,o,o,o,o,o,o,o,o,b],
                 [b,b,b,b,b,b,b,b,b,b],
                 [b,b,b,b,b,b,b,b,b,b],
                 [b,b,b,b,s,b,b,b,b,b],
                 [b,b,b,b,b,b,b,b,b,b]
                 ]
                 
#PSEUDO/PLANNING - create function that translates ascii-character maps of grids
#into text that the program can use
def asciiGridToText(asciiGrid):
    size=len(asciiGrid)
    obstacleLocations=[]
    startLocations=[]
    endLocations=[]
    #iterate over the ascii grid
    for y in range(len(asciiGrid)):
        for x in range(len(asciiGrid)):
            if(asciiGrid[y][x]==o):
                obstacleLocations.append([x,y])
            elif(asciiGrid[y][x]==s):
                startLocations.append([x,y])
            elif(asciiGrid[y][x]==e):
                endLocations.append([x,y])
    return (size, obstacleLocations, startLocations, endLocations)


#PSEUDO/PLANNING - create text of locations for obstacles, cars, and endpoints
def randomGridToText(size, cars, obstacleProb):
    np.random.seed()
    squareProbs=np.random.random_sample(1000000)
    count=0
    obstacleLocations=[]
    startLocations=[]
    endLocations=[]
    #first, place the obstacles
    for y in range(size):
        for x in range(size):
            if(squareProbs[count]<obstacleProb):
                obstacleLocations.append([x,y])
            count+=1
    #next, place the starting positions
    for c in range(cars):
        startX=random.randint(0, size-1)
        startY=random.randint(0, size-1)
        newStart=[startX, startY]
        startLocations.append(newStart)
        try:
            obstacleLocations.index(newStart)
            obstacleLocations.remove(newStart)
        except ValueError:
            pass
    #place the endpoints
    for d in range(cars):
        while True:
            notAtStart=False
            endX=random.randint(0, size-1)
            endY=random.randint(0, size-1)
            newEnd=[endX, endY]
            if(newEnd!=startLocations[d]):
                notAtStart=True
            try:
                obstacleLocations.index(newEnd)
                if(notAtStart):
                    obstacleLocations.remove(newEnd)
                    break
            except ValueError:
                pass
            if(notAtStart):
                break
        endLocations.append(newEnd)
    return (size, obstacleLocations, startLocations, endLocations)
        
        
                
            
            
            
#PSEUDO/PLANNING - creates text of a grid with arbitrarily large parameters
def inputGenerator(filename, size, cars, obstacleProb, asciiGrid=None):
    inputWriter=open(filename, 'w')
    if(asciiGrid==None):
        restOfInfo=randomGridToText(size, cars, obstacleProb)
    else:
        restOfInfo=asciiGridToText(asciiGrid)
    obstacleLocations=restOfInfo[1]
    startLocations=restOfInfo[2]
    endLocations=restOfInfo[3]
    obstacles=len(obstacleLocations)

    #begin writing to file
    inputWriter.write(str(size)+"\n"+str(cars)+"\n"+str(obstacles)+"\n")
    for locationList in obstacleLocations, startLocations, endLocations:
        for location in locationList:
            inputWriter.write(str(location[0])+","+str(location[1])+"\n")
    inputWriter.close()

if __name__=="__main__":
    #inputGenerator("input5.txt", 10, 1, 0.05, asciiGrid=sampleASCIIGrid)
    inputGenerator("input7.txt", 100, 5, 0.1, asciiGrid=None)

