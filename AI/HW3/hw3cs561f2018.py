# CSCI 561, Fall 2018, Homework 3
# Author: Leland Launer
# This program will contain a agent program to
# direct cars across a grid in a stochastic environment

import pdb
import logging
import copy
import math
import numpy as np
import pprint
pp=pprint.PrettyPrinter()
logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s - %(levelname)s- %(message)s')
#logging.disable(logging.CRITICAL)

#####PART 0 - CLASSES, FUNCTIONS, CONSTANTS
INPUT_FILE_NUM="7" #making it a string so that "input.txt" is also doable in the format below
INIT_REWARD=-1.0
INIT_UTILITY=0.0
INIT_ACTION=None
INIT_NODE=None
TERMINAL_REWARD=100.0
OBSTACLE_REWARD=-100.0
GAMMA=0.9
MAX_ERROR=0.1
NORTH="N"
SOUTH="S"
EAST="E"
WEST="W"
ACTIONS={NORTH:(0, -1), SOUTH:(0, 1), EAST:(1, 0), WEST:(-1,0)}
INTENDED_PROBABILITY=0.7
ACCIDENTAL_PROBABILITY=0.1
TIEBREAK_DICT={NORTH:4, SOUTH:3, EAST:2, WEST:1}
TURNS=[NORTH, EAST, SOUTH, WEST]

'''
#COURSE SUPPLIED CODE
orientations = EAST, NORTH, WEST, SOUTH = [(1, 0), (0, -1), (-1, 0), (0, 1)]
turns = LEFT, RIGHT = (+1, -1)
def turn_heading(heading, inc, headings=orientations):
    return headings[(headings.index(heading) + inc) % len(headings)]
def turn_right(heading):
    return turn_heading(heading, RIGHT)
def turn_left(heading):
    return turn_heading(heading, LEFT)
#END OF COURSE SUPPLIED CODE
'''

def TIEBREAK(direction):
    return TIEBREAK_DICT[direction]

def turn_right(direction):
    directionIndex=TURNS.index(direction)
    rightIndex=directionIndex+1
    if(rightIndex>=len(TURNS)):
        rightIndex=0
    return TURNS[rightIndex]

def turn_left(direction):
    directionIndex=TURNS.index(direction)
    leftIndex=directionIndex-1
    return TURNS[leftIndex]

class Car:
    def __init__(self, carNum, startLocation, endLocation):
        self.carNum=carNum
        self.startLocation=copy.copy(startLocation)
        self.endLocation=copy.copy(endLocation)
    def __str__(self):
        return "Car number "+str(self.carNum)+" has starting location "+str(self.startLocation)+" and end location "+str(self.endLocation)
        
class StateNode:
    def __init__(self, reward, utility, maxAction, position):
        self.reward=reward
        self.utility=utility
        self.maxAction=maxAction
        self.position=position
    def __str__(self):
        return str(self.maxAction) #str(round(self.utility))
    
class StateGrid:
    def __init__(self, rewardsGrid, endLocation, size):
        #PSEUDO/PLANNING - store endLocation, since this will be useful for indexing purposes
        #i.e. you can reuse a state grid if the endLocation is the same
        self.endLocation=copy.copy(endLocation)
        self.size=size
        #PSEUDO/PLANNING - create a grid of state nodes with dummy values
        self.grid=[ [INIT_NODE for x in range(size)] for y in range(size)]
        #PSEUDO/PLANNING - update specific state nodes' rewards based on rewardsGrid
        for y in range(size):
            for x in range(size):
                newStateNode=StateNode(rewardsGrid[y][x], INIT_UTILITY, INIT_ACTION, [x,y])
                self.grid[y][x]=newStateNode
        #PSEUDO/PLANNING - update one node's reward based on endLocation
        terminalNode=self.grid[endLocation[1]][endLocation[0]]
        terminalNode.reward+=TERMINAL_REWARD #a bit handwavy here, could create a mutator instead
        terminalNode.utility=terminalNode.reward

    def __str__(self):
        overallGridString=''
        for y in range(self.size):
            rowString=''
            for x in range(self.size):
                rowString+=str(self.grid[y][x])+","
            overallGridString+=rowString+"\n"
        return overallGridString
        
        '''This will implement the value iteration algorithm
        used to discover ideal policies for Markov Decision Processes.
        Once this function finishes, then the state grid will contain
        all the information for the ideal policy in its state nodes,
        in particular in the attribute "maxAction"
        
        gamma is the factor by which future rewards are discounted
        maxError is the the max error allowed for any one utility value from its ideal value
        '''
    def updateUtilitiesOfAllNodes(self, actions, gamma, maxError):
        count=0
        while True:
            logging.debug("This is iteration number %d for policy discovery for end location %s"%(count,self.endLocation))
            #PSEUDO/PLANNING - make a copy of the grid, since you are about to modify it in-place
            oldGrid=copy.deepcopy(self)
            delta=0
            #pdb.set_trace()
            #PSEUDO/PLANNING - update the utility node-by-node and check the change in utility
            for y in range(self.size):
                for x in range(self.size):
                    if (x!=self.endLocation[0] or y!=self.endLocation[1]):
                        changeInUtility=self.updateUtilityOfNode(self.grid[y][x], actions, oldGrid, gamma)
                        if(delta<changeInUtility):
                            delta=changeInUtility
            if(delta<(1-gamma)/gamma*maxError):
                return
            count+=1
                        
    '''This function updates the utility of the givenNode while also
    returning the change in utility between this iteration and that last one.

    Actions is a dictionary of directional keys like "North" and tuple values
    that correspond to the movement they represent (i.e. (0,-1) for "North)
    '''
    def updateUtilityOfNode(self, givenNode, actions, oldGrid, gamma):
        #PSEUDO/PLANNING - BELLMAN UPDATE EQUATION IMPLEMENTED IN THIS FUNCTION
        #PSEUDO/PLANNING - find succesor states of the node
        successors=oldGrid.findSuccessors(givenNode, actions, oldGrid)
        #PSEUDO/PLANNING - calculate the expected utility of each of the four actions
        maxExpectedUtilityAndAction=self.findMaxExpectedUtilityAndAction(givenNode, successors)
        maxExpectedUtility=maxExpectedUtilityAndAction[0]
        maxAction=maxExpectedUtilityAndAction[1]
        #PSEUDO/PLANNING - apply gamma to the max expected utility, then add the reward of this node
        overallUtility=gamma*maxExpectedUtility+givenNode.reward
        changeInUtility=abs(overallUtility-givenNode.utility)
        #PSEUDO/PLANNING - record this number as the new utility of the node
        givenNode.utility=overallUtility
        #PSEUDO/PLANNING - record the action that gave the max expected utility as maxAction of givenNode
        givenNode.maxAction=maxAction
        #PSEUDO/PLANNING - lastly, return the change in values between the current and previous utilites
        return changeInUtility
        
    '''findSuccessors returns successors, which is a dict of four successors (i.e. neighboring squares in the grid),
    even if, say, the corners of the grid technically just have two neighbors.
    In that case, two of the successors will just be the node itself'''
    def findSuccessors(self, givenNode, actions, oldGrid):
        successors={}
        for actionKey in actions.keys():
            action=actions[actionKey]
            successorPosition=[givenNode.position[0]+action[0],givenNode.position[1]+action[1]]
            if(oldGrid.size>successorPosition[0]>=0 and oldGrid.size>successorPosition[1]>=0):
                successorNode=oldGrid.grid[successorPosition[1]][successorPosition[0]]
            else: #if that is the case, just revert back to the node in the oldGrid analogous to the givenNode
                successorNode=oldGrid.grid[givenNode.position[1]][givenNode.position[0]]
            successors[actionKey]=successorNode
        return successors
    '''This function returns a list of two items.
    First item is the max expected utility, a float.
    Second item is the max action, which is string of the direction for the car to drive in.
    Might consolidate this into a new class of objects
    '''
    def findMaxExpectedUtilityAndAction(self, givenNode, successors):
        #PSEUDO/PLANNING - for each of the successors, calculate the expected utility of the action
        expectedUtilities={}
        for intendedAction in successors.keys():
            expectedUtility=0
            intendedSuccessorUtility=successors[intendedAction].utility*INTENDED_PROBABILITY
            expectedUtility+=intendedSuccessorUtility
            for accidentalAction in successors.keys():
                if (intendedAction!=accidentalAction):
                    accidentalSuccessorUtility=successors[accidentalAction].utility*ACCIDENTAL_PROBABILITY
                    expectedUtility+=accidentalSuccessorUtility
            expectedUtilities[intendedAction]=expectedUtility
        #PSEUDO/PLANNING - find the max here? possibly implement tiebreaking here?
        maxAction=expectedUtilities.keys()[0] #loop and a half structure
        maxExpectedUtility=expectedUtilities[maxAction]
        for intendedAction in expectedUtilities.keys():
            if (maxAction==None) or (maxExpectedUtility<expectedUtilities[intendedAction]):
                maxAction=intendedAction
                maxExpectedUtility=expectedUtilities[intendedAction]
            elif(maxExpectedUtility==expectedUtilities[intendedAction] and TIEBREAK(intendedAction)>TIEBREAK(maxAction)): #TIEBREAKING PROTOCOL HERE
                maxAction=intendedAction
                maxExpectedUtility=expectedUtilities[intendedAction]
        '''
        if(givenNode.position[0]==0 and givenNode.position[1]==0):
           logging.debug("EXPECTED UTILITES FOR %s: %s"%(givenNode.position, expectedUtilities))
        '''
        return [maxExpectedUtility, maxAction]

'''Performs ten trials for each car specified in carsAndGrids.
carsAndGrids is merely a list of tuples with the format (car, grid),
where grid is a StateGrid instance.
Returns a list of average rewards, one for each car, in the order which they appear in carsAndGrids.'''
def performSimulation(carsAndGrids):
    avgRewards=[]
    for carAndGrid in carsAndGrids:
        car=carAndGrid[0]
        grid=carAndGrid[1]
        rewards=np.zeros(10)
        logging.debug("CCCCCCCCCCCCCCCCCCCCCCCC FOR CAR NUMBER %d CCCCCCCCCCCCCCCCCCCCCCCC"%(car.carNum))
        for j in range(10):
            #logging.debug("%d~~~~~~~~~~~~~~~~~~~~~~~~~"%(j))
            np.random.seed(j)
            swerve = np.random.random_sample(1000000)
            pos=copy.deepcopy(car.startLocation)
            reward=0
            k=0
            while (pos[0]!=car.endLocation[0] or pos[1]!=car.endLocation[1]):
                move = grid.grid[pos[1]][pos[0]].maxAction
                if swerve[k] > 0.7:
                    if swerve[k] > 0.8:
                        if swerve[k] > 0.9:
                            move = turn_right(turn_right(move))
                        else:
                            move = turn_right(move)
                    else:
                        move = turn_left(move)
                #now, actually use the move to change the position
                displace=ACTIONS[move]
                if(grid.size>(pos[1]+displace[1])>=0 and grid.size>(pos[0]+displace[0])>=0):
                    pos[1]+=displace[1]
                    pos[0]+=displace[0]
                #add the reward of this new state
                curRew=grid.grid[pos[1]][pos[0]].reward
                reward+=curRew
                #logging.debug("MOVE:\t%s\tVALUE GAINED:\t%f\tSWERVE VALUE:%f"%(displace, curRew, swerve[k]))
                k+=1
            #pdb.set_trace()
            rewards[j]=reward
            #logging.debug("SCORE: %s"%rewards)
        avgReward=np.average(rewards)
        floorAvRew=math.floor(avgReward)
        avgRewards.append(int(floorAvRew))
        logging.debug("For car number %d, the average is %f"%(car.carNum, floorAvRew))
    return avgRewards
#####PART 1 - SETUP
if __name__=="__main__":
    #PSEUDO/PLANNING - create file object for input.txt
    inputReader=open("input%s.txt"%INPUT_FILE_NUM, "r")
    obstaclePositions=[]
    startLocations=[]
    endLocations=[]
    cars=[]
    s=int(inputReader.readline()) #grid dimension
    n=int(inputReader.readline()) #number of cars on the grid
    o=int(inputReader.readline()) #number of obstacles
    for uselessIndex in range(o): #obtain list of obstacle positions
        obstaclePosition=map(int, inputReader.readline().rstrip().split(","))
        obstaclePositions.append(obstaclePosition)
    for uselessIndex in range(n): #obtain list of starting positions for the cars
        startLocation=map(int, inputReader.readline().rstrip().split(","))
        startLocations.append(startLocation)
    for uselessIndex in range(n):
        endLocation=map(int, inputReader.readline().rstrip().split(","))
        endLocations.append(endLocation)
    inputReader.close()

    #PSEUDO/PLANNING - create master rewards grid that all policies will need to use
    rewardsRow=np.array([INIT_REWARD for i in range(s)], dtype=int)
    masterRewardsGrid=np.array([rewardsRow.copy() for i in range(s)], dtype=int)
    for obstaclePosition in obstaclePositions:
        masterRewardsGrid[obstaclePosition[1]][obstaclePosition[0]]+=OBSTACLE_REWARD
    #PSEUDO/PLANNING - create Car objects from starting and ending locations
    for carNum in range(n):
        newCar=Car(carNum, startLocations[carNum], endLocations[carNum])
        cars.append(newCar)

    #PSEUDO/PLANNING - find the optimal policy for each car
    carsAndGrids=[]
    for car in cars:
        #PSEUDO/PLANNING - check to see if there is already a state grid with the same terminal position
        #PSEUDO/PLANNING - if there isn't one, make a new state grid
        newStateGrid=StateGrid(masterRewardsGrid, car.endLocation, s)
        newStateGrid.updateUtilitiesOfAllNodes(ACTIONS, GAMMA, MAX_ERROR)
        logging.debug("Here is the state grid for car number %d:"%(car.carNum))
        print(str(newStateGrid))
        carsAndGrids.append([car, newStateGrid])

    #perform the simulations
    avgRewards=performSimulation(carsAndGrids)


    logging.debug("Here is the grid dimension: %d"%s)
    logging.debug("Here is the number of cars: %d"%n)
    logging.debug("Here is the number of obstacles: %d"%o)
    logging.debug("Here are the obstacle positions: "+str(obstaclePositions))
    logging.debug("Here are the start locations: "+str(startLocations))
    logging.debug("Here are the end locations: "+str(endLocations))
    logging.debug("Here is the master rewards grid(just obstacles):\n"+str(masterRewardsGrid))
    logging.debug("Here are the cars:")
    for car in cars:
        logging.debug(str(car))
    
    outputWriter=open("output.txt","w")
    for i in range(len(avgRewards)):
        outputWriter.write(str(avgRewards[i]))
        if(i<(len(avgRewards)-1)):
            outputWriter.write("\n")
    outputWriter.close()

    '''
    for direction in TURNS:
        logging.debug("direction=%s , turn_left(direction)=%s, turn_left(turn_left(direction))=%s, turn_right(direction)=%s"%(direction, turn_left(direction), turn_left(turn_left(direction)), turn_right(direction)))


  testInputReader=open("policy%d.txt"%INPUT_FILE_NUM)
    for carAndGrid in carsAndGrids:
        car=carAndGrid[0]
        grid=carAndGrid[1]
        logging.debug("Here are the needed corrections for the policy related to car number: %d"%car.carNum)
        for i in range(s*s):
            line=testInputReader.readline().split(":")
            gridPositionStr=line[0].strip()
            gridX=int(gridPositionStr[1])
            gridY=int(gridPositionStr[4])
            expectedMaxActionStr=line[1].strip()
            currentMaxAction=grid.grid[gridY][gridX].maxAction
            if(currentMaxAction==None):
                currentMaxActionStr=str(currentMaxAction)
            else:
                currentMaxActionStr=str(ACTIONS[currentMaxAction])
            if(currentMaxActionStr!=expectedMaxActionStr):
                logging.debug("For "+gridPositionStr+", the maxAction should be "+expectedMaxActionStr+" instead of "+currentMaxActionStr)

    testInputReader.close()

'''
    
           
        
        
    
    
        
