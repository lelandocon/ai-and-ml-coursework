"""
Do not change the input and output format.
If our script cannot run your code or the format is improper, your code will not be graded.

The only functions you need to implement in this template is linear_regression_noreg, linear_regression_invertible，regularized_linear_regression,
tune_lambda, test_error and mapping_data.
"""

import numpy as np
import pandas as pd

###### Q1.1 ######
def mean_square_error(w, X, y):
    """
    Compute the mean squre error on test set given X, y, and model parameter w.
    Inputs:
    - X: A numpy array of shape (num_samples, D) containing test feature.
    - y: A numpy array of shape (num_samples, ) containing test label
    - w: a numpy array of shape (D, )
    Returns:
    - err: the mean square error
    """
    # PSEUDO/PLAN: calculate the squared errors by iterating through the examples
    num_samples=X.shape[0]
    square_errors=[]
    for sampleIndex in range(num_samples):
        sample=X[sampleIndex]
        #TODO : check dimensions of the sample and w before multiplying, and y before subtracting
        square_errors.append((y[sampleIndex]-np.dot(w, sample))**2)
    # PSEUDO/PLAN: take the mean of the squared errors
    return np.mean(square_errors)

###### Q1.2 ######
def linear_regression_noreg(X, y):
  """
  Compute the weight parameter given X and y.
  Inputs:
  - X: A numpy array of shape (num_samples, D) containing feature.
  - y: A numpy array of shape (num_samples, ) containing label
  Returns:
  - w: a numpy array of shape (D, )
  """
  #TODO : check dimensions of X, y, z. Best way to do so?
  #PSEUDO/PLAN : merely use the equation w*=(Xtranspose*X)^inverse * Xtranspose*Y
  covariance=np.matmul(np.transpose(X), X)
  invOfCovariance=np.linalg.inv(covariance)
  best_w=np.matmul(invOfCovariance, np.matmul(np.transpose(X),y))
  return best_w

###### Q1.3 ######
def linear_regression_invertible(X, y):
    """
    Compute the weight parameter given X and y.
    Inputs:
    - X: A numpy array of shape (num_samples, D) containing feature.
    - y: A numpy array of shape (num_samples, ) containing label
    Returns:
    - w: a numpy array of shape (D, )
    """
    CORRECTIVE_QUANTUM=0.1
    totalCorrection=0.0
    # PSEUDO/PLAN : do eigendecomposition on the covariance
    covariance=np.matmul(np.transpose(X),X)
    eival, eivec = np.linalg.eig(covariance)
    # PSEUDO/PLAN : find the minimum of absolute eigenvalues
    minEival=np.min(np.abs(eival))
    # PSEUDO/PLAN : while the matrix is non-invertible i.e. the minimum eigenvalue is less than 10^-5
    # then keep adding 10^-1 * Identity matrix
    while(minEival<10**(-5)):
        eival+= CORRECTIVE_QUANTUM
        totalCorrection += CORRECTIVE_QUANTUM
        minEival = np.min(np.abs(eival))
    # PSEUDO/PLAN : compute the inverse of the covariance with (Covar)inverse=(eivec)transpose*(eival)inverse*eivec
    eivalMatrix=np.identity(eival.size)
    correctionMatrix=np.identity(eival.size)
    for eivalIndex in range(eival.size):
        eivalMatrix[eivalIndex]*=eival[eivalIndex]
        correctionMatrix[eivalIndex] *= totalCorrection
    newCovariance=covariance+correctionMatrix
    #invOfCovariance=np.matmul(np.transpose(eivec), np.matmul(np.linalg.inv(eivalMatrix), eivec))
    invOfCovariance = np.linalg.inv(newCovariance)
    best_w = np.matmul(invOfCovariance, np.matmul(np.transpose(X), y))
    return best_w


###### Q1.4 ######
def regularized_linear_regression(X, y, lambd):
    """
    Compute the weight parameter given X, y and lambda.
    Inputs:
    - X: A numpy array of shape (num_samples, D) containing feature.
    - y: A numpy array of shape (num_samples, ) containing label
    - lambd: a float number containing regularization strength
    Returns:
    - w: a numpy array of shape (D, )
    """
    # PSEUDO/PLAN : use the formula best_w=(covariance-lambda*identity)inverse * Xtranspose*y
    covariance = np.matmul(np.transpose(X), X)
    correctedCovariance=covariance+lambd*np.identity(covariance.shape[0])
    invOfCovariance = np.linalg.inv(correctedCovariance)
    best_w = np.matmul(invOfCovariance, np.matmul(np.transpose(X), y))
    return best_w

###### Q1.5 ######
def tune_lambda(Xtrain, ytrain, Xval, yval):
    """
    Find the best lambda value.
    Inputs:
    - Xtrain: A numpy array of shape (num_training_samples, D) containing training feature.
    - ytrain: A numpy array of shape (num_training_samples, ) containing training label
    - Xval: A numpy array of shape (num_val_samples, D) containing validation feature.
    - yval: A numpy array of shape (num_val_samples, ) containing validation label
    Returns:
    - bestlambda: the best lambda you find in lambds
    """
    # PSEUDO/PLAN : iterate over orders of 10 to find a suitable lambda
    bestlambda = None
    bestlambdPower = None
    bestErr = 0.0 #dummy value
    for lambdPower in range(-19,20):
        lambd=10**(lambdPower)
        lambdWeights=regularized_linear_regression(Xtrain, ytrain, lambd)
        lambdErr=mean_square_error(lambdWeights, Xval, yval)
        if bestlambda==None or lambdErr<bestErr:
            bestlambda=lambd
            bestErr=lambdErr
            bestlambdPower=lambdPower
    return 10**(bestlambdPower)
    

###### Q1.6 ######
def mapping_data(X, power):
    """
    Mapping the data.
    Inputs:
    - X: A numpy array of shape (num_training_samples, D) containing training feature.
    - power: A integer that indicate the power in polynomial regression
    Returns:
    - X: mapped_X, You can manully calculate the size of X based on the power and original size of X
    """
    num_training_samples=X.shape[0]
    # PSEUDO/PLAN : for each power 2 or greater, make a new section of the X array
    for curPower in range(2, power+1):
        # PSEUDO/PLAN : iterate over the original entries of the array via index
        for sampleIndex in range(num_training_samples):
            sample=X[sampleIndex]
            newSample=sample**(curPower)
            np.append(X, [newSample], axis=0)
    return X


