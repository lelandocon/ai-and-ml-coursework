import numpy as np

'''
Collaboration/citations

For multiclass train, I used Kahlil Dozier's recommendation to create a separate softmax function
'''
def binary_train(X, y, loss="perceptron", w0=None, b0=None, step_size=0.5, max_iterations=1000):
    """
    Inputs:
    - X: training features, a N-by-D numpy array, where N is the 
    number of training points and D is the dimensionality of features
    - y: binary training labels, a N dimensional numpy array where 
    N is the number of training points, indicating the labels of 
    training data
    - loss: loss type, either perceptron or logistic
    - step_size: step size (learning rate)
	- max_iterations: number of iterations to perform gradient descent

    Returns:
    - w: D-dimensional vector, a numpy array which is the weight 
    vector of logistic or perceptron regression
    - b: scalar, which is the bias of logistic or perceptron regression
    """
    ARBITRARY_THRESHOLD=0
    N, D = X.shape
    assert len(np.unique(y)) == 2

    # PSEUDO/PLAN: change the training data so that the labels are {-1,1} instead of {0,1}
    y[y==0]=-1  # numpy array indexing works well here


    w = np.zeros(D)
    if w0 is not None:
        w = w0
    
    b = 0
    if b0 is not None:
        b = b0

    if loss == "perceptron":
        #note: I refer to the gradient below as derivatives of the loss with respect to the weights in w.
        # I will calculate the partial derivative of the loss with respect to the bias separately
        avgGradient=np.array([ARBITRARY_THRESHOLD+1]) #dummy value
        avgPartialDeriv=ARBITRARY_THRESHOLD+1 #dummy value
        iterations=0
        # PSEUDO/PLAN: update the weights until it converges (i.e. change is underneath arbitrary threshold)
        while iterations<max_iterations:
            gradient=np.zeros(D)
            partialDerivateForBias=0.0
            # PSEUDO/PLAN:calculate the gradient at this point
            # PSEUDO/PLAN: iterate over all the examples
            for sampleIndex in range(N):
                sample=X[sampleIndex]
                sampleLabel=y[sampleIndex]
                predictedLabel=np.dot(w, sample)+b
                z=sampleLabel*predictedLabel
                if(z<=0): #if this sample was misclassified,

                    #change the weights and bias according the the data point and label
                    partOfGradient=-sampleLabel*sample
                    partOfBiasDeriv=-sampleLabel*1

                    #add these two parts to their respective wholes
                    gradient += partOfGradient
                    partialDerivateForBias += partOfBiasDeriv
            # PSEUDO/PLAN: take the mean of these sample gradients i.e. obtain average gradient descent
            avgGradient=(1/N)*gradient
            avgPartialDeriv=(1/N)*partialDerivateForBias
            # PSEUDO/PLAN: finally, update the weights and bias
            w=w-step_size*avgGradient
            b=b-step_size*avgPartialDeriv
            iterations+=1
        '''
        for iteration in range(max_iterations):
            activations=np.dot(w, X.T)+b
            z=y*activations
            gradientForWeights=z[z<=0]
            gradientForBias=0
            '''



        

    elif loss == "logistic":
        #          Compute w and b here            #
        w = np.zeros(D)
        b = 0

        #append b to w
        newW=np.append(w, b)
        newX = np.insert(X, D, 1, axis=1)
        newXTrans=newX.T
        strangeY=np.repeat(y, D+1).reshape((N, D + 1))
        labelTimesPoint = strangeY * newX  # TODO: check dimensions of matrix


        # PSEUDO/PLAN: update the weights until it converges (i.e. change is underneath arbitrary threshold)
        # PLAN: iterate for max_iterations
        for iteration in range(max_iterations):
            # PLAN: find the gradient for logistic loss
            # PLAN: calculate the activation function yn * wT *x
            activations=np.dot(newW, newXTrans)*y
            sigmoidActs=sigmoid(-activations)
            gradient=-np.matmul(sigmoidActs.T, labelTimesPoint)
            avgGradient=gradient/float(N)
            newW=newW-step_size*avgGradient

        w=newW[:-1]
        b=newW[-1]
        

    else:
        raise "Loss Function is undefined."

    assert w.shape == (D,)
    y[y==-1]=0 #now that the training is done, return labels to original status
    return w, b

def sigmoid(z):
    
    """
    Inputs:
    - z: a numpy array or a float number
    
    Returns:
    - value: a numpy array or a float number after computing sigmoid function value = 1/(1+exp(-z)).
    """

    value=1.0/(1.0+np.exp(-z))
    
    return value

def binary_predict(X, w, b, loss="perceptron"):
    """
    Inputs:
    - X: testing features, a N-by-D numpy array, where N is the 
    number of training points and D is the dimensionality of features
    - w: D-dimensional vector, a numpy array which is the weight 
    vector of your learned model
    - b: scalar, which is the bias of your model
    - loss: loss type, either perceptron or logistic
    
    Returns:
    - preds: N dimensional vector of binary predictions: {0, 1}
    """
    N, D = X.shape
    FIRST_CLASS=1
    SECOND_CLASS=0
    thresholdForPrediction= None
    
    if loss == "perceptron":
        #          Compute preds                   #
        preds = np.zeros(N)
        thresholdForPrediction=0
        for sampleIndex in range(N):
            sample=X[sampleIndex]
            prediction = np.dot(w, sample) + b
            if(prediction>thresholdForPrediction):
                preds[sampleIndex]=FIRST_CLASS
            else:
                preds[sampleIndex]=SECOND_CLASS
        

    elif loss == "logistic":
        #          Compute preds                   #
        preds = np.zeros(N)
        thresholdForPrediction = 0.5
        for sampleIndex in range(N):
            sample=X[sampleIndex]
            prediction=sigmoid(np.dot(w, sample)+b)
            if(prediction>thresholdForPrediction):
                preds[sampleIndex]=FIRST_CLASS
            else:
                preds[sampleIndex]=SECOND_CLASS
        

    else:
        raise "Loss Function is undefined."
    

    assert preds.shape == (N,) 
    return preds



def multiclass_train(X, y, C,
                     w0=None, 
                     b0=None,
                     gd_type="sgd",
                     step_size=0.5, 
                     max_iterations=1000):
    """
    Inputs:
    - X: training features, a N-by-D numpy array, where N is the 
    number of training points and D is the dimensionality of features
    - y: multiclass training labels, a N dimensional numpy array where
    N is the number of training points, indicating the labels of 
    training data
    - C: number of classes in the data
    - gd_type: gradient descent type, either GD or SGD
    - step_size: step size (learning rate)
    - max_iterations: number of iterations to perform gradient descent

    Returns:
    - w: C-by-D weight matrix of multinomial logistic regression, where 
    C is the number of classes and D is the dimensionality of features.
    - b: bias vector of length C, where C is the number of classes
    """

    N, D = X.shape

    w = np.zeros((C, D))
    if w0 is not None:
        w = w0
    
    b = np.zeros(C)
    if b0 is not None:
        b = b0

    np.random.seed(42)
    if gd_type == "sgd":
        #          Compute w and b                 #
        w = np.zeros((C, D))
        #b = np.zeros(C)
        # PLAN : append the bias onto the weights matrix as a row that each weight vector will access to
        # this will make calculations easier
        newW = np.insert(w, D, 0, axis=1) #TODO: change back to D-1?

        # PLAN : iterate for max_iterations
        indexArray=np.arange(N)
        for iterationNum in range(max_iterations):
            # PLAN : choose a random point, identify its label
            randomSampleIndex=np.random.choice(indexArray)
            randomSample=X[randomSampleIndex]
            randomLabel=y[randomSampleIndex]
            # PLAN : calculate the gradient i.e. create a vector of softmax functions that depend on the label
            gradient=gradient_with_softmax(newW, randomSample, randomLabel, C, D)
            # PLAN : update the weight matrix with the gradient
            newW=newW-step_size*gradient

        # PLAN: Convert newW back into w and b
        w=newW[...,0:-1]
        b=newW[...,-1]

    elif gd_type == "gd":
        #          Compute w and b                 #
        w = np.zeros((C, D))
        b = np.zeros(C)

        #PLAN: create CXN matrix for the labels, where a 1 value means that the sample represented by that column
        # belongs to the class represented by that row
        ymatrix=np.zeros((C,N))
        for labelIndex in range(y.shape[0]):
            label=y[labelIndex]
            ymatrix[label][labelIndex]=1


        newW = np.insert(w, D, 0, axis=1)  # TODO: change back to D-1?
        newX = np.insert(X, D, 1, axis=1)
        newXTrans=newX.T
        # PLAN: iterate for max iterations
        for iteration in range(max_iterations):
            # PLAN: calculate the gradient for all
            # PLAN: multiply newW(C by D+1 matrix) by newX Transpose (D+1 by N matrix) to get the activations
            activations=np.dot(newW, newXTrans)
            stablerActivations=activations-np.max(activations, axis=0) #TODO: revisit this subtraction
            denomsForSoftMax=np.sum(np.exp(stablerActivations), axis=0)
            probabilites=np.exp(stablerActivations)/denomsForSoftMax
            probabilites-=ymatrix #this step accounts for the gradient where k=yn
            gradient=np.matmul(probabilites, newX)
            avgGradient=gradient/N
            #PLAN: update the weights and biases
            newW=newW-step_size*avgGradient


        # PLAN: Convert newW back into w and b
        w = newW[..., 0:-1]
        b = newW[..., -1]
        

    else:
        raise "Type of Gradient Descent is undefined."
    

    assert w.shape == (C, D)
    assert b.shape == (C,)

    return w, b

def gradient_with_softmax(newW, x, y, C, D):
    #newW is a weights matrix with each column being a weight vector of a given class and with the bias row appended
    #newW has the shape C by D+1
    #newx is a single example/point from the data with a 1 appended to it for the bias
    #y is the class label of that single example
    # PLAN : make an array with the dot product of each weight vector in the matrix with x
    '''
    weightsDotPoint=np.zeros(W.shape[0])
    for weightVectorIndex in range(weightsDotPoint.shape[0]):
        weightVector=W[weightVectorIndex]
        weightDotPoint=np.dot(weightVector, x)
        weightsDotPoint[weightVectorIndex]=weightDotPoint
        '''
    newx = np.append(x, 1) #necessary transformation for dot product with newW
    weightsDotPoint=np.dot(newW, newx) #this is a C by 1 vector of dot products between a weight vector and the single point

    # PLAN : find the denominator for the softmax function, which is the sum of exponents raised to a gvien weightDotPoint
    stablerWeightsDotPoint=weightsDotPoint-np.max(weightsDotPoint) #this change helps with the underflow/overflow of np.exp
    denomForSoftMax=np.sum(np.exp(stablerWeightsDotPoint))
    newerx=np.expand_dims(newx, axis=0)
    probabilities=np.expand_dims(np.exp(stablerWeightsDotPoint)/denomForSoftMax, axis=1)
    '''
    probabilities = np.array([])
    for weightPoint in weightsDotPoint:
        probability = np.exp(weightPoint - np.max(weightsDotPoint)) / np.sum(np.exp(weightsDotPoint - np.max(weightsDotPoint)))
        probabilities = np.append(probabilities, probability)
    probabilities=np.expand_dims(probabilities, axis=1)
    '''

    # PLAN : calculate the gradient
    gradient= np.matmul(probabilities, newerx)

    # PLAN : REMEMBER, the derivative with respect to the weight vector associated with y (x's label) is slightly different.
    # it is the softmax of y=x's class - 1. Since we already did a dot product with x, we simply subtract x from that
    # specific part of the gradient indexed by y
    gradient[y]-=newx

    return gradient #THIS GRADIENT HAS DIMENSIONS C by D+1, BECAUSE OF THE BIAS VECTOR APPENDED EARLIER

def multiclass_predict(X, w, b):
    """
    Inputs:
    - X: testing features, a N-by-D numpy array, where N is the 
    number of training points and D is the dimensionality of features
    - w: weights of the trained multinomial classifier, C-by-D 
    - b: bias terms of the trained multinomial classifier, length of C
    
    Returns:
    - preds: N dimensional vector of multiclass predictions.
    Outputted predictions should be from {0, C - 1}, where
    C is the number of classes
    """
    N, D = X.shape
    #          Compute preds                   #
    C=b.shape[0]
    reshapedB=np.repeat(b, N).reshape((C,N))
    #PLAN: compute the C by N matrix of dot products of each point with each class vector in the weights
    similarities=np.matmul(w,X.T)+reshapedB
    preds=np.argmax(similarities, axis=0).astype(float)


    assert preds.shape == (N,)
    return preds




        