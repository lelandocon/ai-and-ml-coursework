import numpy as np


def get_k_means_plus_plus_center_indices(n, n_cluster, x, generator=np.random):
    '''

    :param n: number of samples in the data
    :param n_cluster: the number of cluster centers required
    :param x: data-  numpy array of points
    :param generator: random number generator from 0 to n for choosing the first cluster at random
            The default is np.random here but in grading, to calculate deterministic results,
            We will be using our own random number generator.


    :return: the center points array of length n_clusters with each entry being index to a sample
             which is chosen as centroid.
    '''

    # implement the Kmeans++ algorithm of how to choose the centers according to the lecture and notebook
    # Choose 1st center randomly and use Euclidean distance to calculate other centers.

    # PLAN : choose the first center via generator, as suggested above
    centers=np.array([], dtype=int)
    centers=np.append(centers,generator.randint(n))
    u = np.expand_dims(x[centers, :], axis=0) #loop-and-a-half, to ensure the first iteration has shape (1,D)

    # PLAN : loop for k-1 times
    for i in range(1, n_cluster):
        # PLAN : calculate the distances from each point to each cluster center
        uMinusX = u - np.expand_dims(x, axis=1)
        l2 = np.linalg.norm(uMinusX, axis=2)

        # PLAN : take the minimum distance for each point
        minima=np.min(l2, axis=1)

        # PLAN : take the argmax of the minima array, which will be the next center
        newCenter=np.argmax(minima)
        centers=np.append(centers, newCenter)

        # PLAN : update the center array
        u = x[centers, :]


    #NOTE: I will be converting the array to a list to address a bug in the grading script
    centers=centers.tolist()
    

    # DO NOT CHANGE CODE BELOW THIS LINE

    print("[+] returning center for [{}, {}] points: {}".format(n, len(x), centers))
    return centers



def get_lloyd_k_means(n, n_cluster, x, generator):
    return generator.choice(n, size=n_cluster)




class KMeans():

    '''
        Class KMeans:
        Attr:
            n_cluster - Number of cluster for kmeans clustering (Int)
            max_iter - maximum updates for kmeans clustering (Int)
            e - error tolerance (Float)
            generator - random number generator from 0 to n for choosing the first cluster at random
            The default is np.random here but in grading, to calculate deterministic results,
            We will be using our own random number generator.
    '''
    def __init__(self, n_cluster, max_iter=100, e=0.0001, generator=np.random):
        self.n_cluster = n_cluster
        self.max_iter = max_iter
        self.e = e
        self.generator = generator

    def fit(self, x, centroid_func=get_lloyd_k_means):

        '''
            Finds n_cluster in the data x
            params:
                x - N X D numpy array
                centroid_func - To specify which algorithm we are using to compute the centers(Lloyd(regular) or Kmeans++)
            returns:
                A tuple
                (centroids a n_cluster X D numpy array, y a length (N,) numpy array where cell i is the ith sample's assigned cluster, number_of_updates a Int)
            Note: Number of iterations is the number of time you update the assignment
        '''
        assert len(x.shape) == 2, "fit function takes 2-D numpy arrays as input"
        
        N, D = x.shape

        self.centers = centroid_func(len(x), self.n_cluster, x, self.generator)


        # - comment/remove the exception.
        # - Initialize means by picking self.n_cluster from N data points
        # - Update means and membership until convergence or until you have made self.max_iter updates.
        # - return (means, membership, number_of_updates)

        # DONOT CHANGE CODE ABOVE THIS LINE

        # PLAN : initialize J as an absurdly large value
        j=10^9

        # PLAN : initialize the (n_clusters by D) "centroids" array from the info inside the (n_clusters by 1) "centers" array
        u=np.array([x[centerIndex] for centerIndex in self.centers])

        # PLAN : loop for max iterations
        for iter_num in range(self.max_iter):
            # PLAN : compute membership to clusters
            uMinusX=u-np.expand_dims(x, axis=1)
            l2=np.linalg.norm(uMinusX, axis=2)
            r_indices=np.argmin(l2, axis=1)

            # PLAN : create a one-hot matrix representing the assignments
            # the one-hot matrix should have dimensions (n by n_clusters)
            # NOTE: this solution was obtained from https://stackoverflow.com/questions/29831489/convert-array-of-indices-to-1-hot-encoded-numpy-array

            r=np.zeros((N, self.n_cluster))
            r[np.arange(N), r_indices]=1

            # PLAN : compute the new means
            uComponents=np.dot(r.T, x)
            uFactor=np.sum(r, axis=0)
            u=uComponents/np.expand_dims(uFactor, axis=1)

            # PLAN : compute J new
            uMinusX = u - np.expand_dims(x, axis=1)
            l2 = np.linalg.norm(uMinusX, axis=2)
            l2_chosen= l2[np.arange(N), r_indices]
            j_new=np.sum(l2_chosen)
            # PLAN : compare J to J new, and if the difference is small, break the loop
            if(np.abs(j-j_new)<self.e):
                break
            # PLAN : set J to J new
            j=j_new

        centroids=u #I was using the symbols in the formulae in the notebook
        y=r_indices
        # DO NOT CHANGE CODE BELOW THIS LINE
        return centroids, y, self.max_iter

        


class KMeansClassifier():

    '''
        Class KMeansClassifier:
        Attr:
            n_cluster - Number of cluster for kmeans clustering (Int)
            max_iter - maximum updates for kmeans clustering (Int)
            e - error tolerance (Float)
            generator - random number generator from 0 to n for choosing the first cluster at random
            The default is np.random here but in grading, to calculate deterministic results,
            We will be using our own random number generator.
    '''

    def __init__(self, n_cluster, max_iter=100, e=1e-6, generator=np.random):
        self.n_cluster = n_cluster
        self.max_iter = max_iter
        self.e = e
        self.generator = generator


    def fit(self, x, y, centroid_func=get_lloyd_k_means):
        '''
            Train the classifier
            params:
                x - N X D size  numpy array
                y - (N,) size numpy array of labels
                centroid_func - To specify which algorithm we are using to compute the centers(Lloyd(regular) or Kmeans++)

            returns:
                None
            Stores following attributes:
                self.centroids : centroids obtained by kmeans clustering (n_cluster X D numpy array)
                self.centroid_labels : labels of each centroid obtained by
                    majority voting (N,) numpy array)
        '''

        assert len(x.shape) == 2, "x should be a 2-D numpy array"
        assert len(y.shape) == 1, "y should be a 1-D numpy array"
        assert y.shape[0] == x.shape[0], "y and x should have same rows"

        self.generator.seed(42)
        N, D = x.shape

        # - comment/remove the exception.
        # - Implement the classifier
        # - assign means to centroids
        # - assign labels to centroid_labels

        # DONOT CHANGE CODE ABOVE THIS LINE

        # PLAN : repeat much of the code used in Kmeans.fit() for obtaining the assignments and the centroids
        kMeansObj=KMeans(self.n_cluster, self.max_iter, self.e, self.generator)
        centroids, assignedClusters, num_updates=kMeansObj.fit(x)

        # PLAN : perform majority vote for all the points in a cluster to obtain centroid labels
        #the centroid_label_counts array is of dimensions num_unique_labels by 2 by n_clusters
        centroid_label_counts=[np.unique(y[assignedClusters==i], return_counts=True) for i in range(self.n_cluster)]
        centroid_labels=np.array([centroid_label_count[0][np.argmax(centroid_label_count[1], axis=0)] for centroid_label_count in centroid_label_counts])

        #TODO: account for the possibility that the cluster doesn't have any points in it and should be assigned a label of zero

        '''
        num_unique_labels=len(np.unique(y))
        data_labels_per_cluster=[[] for i in range(self.n_cluster)]
        centroid_labels=np.array([])
        '''

        # PLAN : store these values in the appropriate attributes
        
        # DONOT CHANGE CODE BELOW THIS LINE

        self.centroid_labels = centroid_labels
        self.centroids = centroids

        assert self.centroid_labels.shape == (
            self.n_cluster,), 'centroid_labels should be a numpy array of shape ({},)'.format(self.n_cluster)

        assert self.centroids.shape == (
            self.n_cluster, D), 'centroid should be a numpy array of shape {} X {}'.format(self.n_cluster, D)

    def predict(self, x):
        '''
            Predict function
            params:
                x - N X D size  numpy array
            returns:
                predicted labels - numpy array of size (N,)
        '''

        assert len(x.shape) == 2, "x should be a 2-D numpy array"

        self.generator.seed(42)
        N, D = x.shape

        # - comment/remove the exception.
        # - Implement the prediction algorithm
        # - return labels

        # DONOT CHANGE CODE ABOVE THIS LINE

        # PLAN : get an l2 array and assign the new data points to a centroid/cluster via argmin
        uMinusX = self.centroids - np.expand_dims(x, axis=1)
        l2 = np.linalg.norm(uMinusX, axis=2)
        r_indices = np.argmin(l2, axis=1)

        # PLAN : translate the assigned centroid for each point into a label for each point via centroid labels
        labels=[self.centroid_labels[centroid_index] for centroid_index in r_indices]
        
        
        # DO NOT CHANGE CODE BELOW THIS LINE
        return np.array(labels)
        

def transform_image(image, code_vectors):
    '''
        Quantize image using the code_vectors

        Return new image from the image by replacing each RGB value in image with nearest code vectors (nearest in euclidean distance sense)

        returns:
            numpy array of shape image.shape
    '''

    assert image.shape[2] == 3 and len(image.shape) == 3, \
        'Image should be a 3-D array with size (?,?,3)'

    assert code_vectors.shape[1] == 3 and len(code_vectors.shape) == 2, \
        'code_vectors should be a 2-D array with size (?,3)'


    # - comment/remove the exception
    # - implement the function

    # DONOT CHANGE CODE ABOVE THIS LINE

    # PLAN : use very similar code to Kmeans.fit to find the closest code vector for each pixel
    uMinusX = code_vectors - np.expand_dims(image, axis=2)
    l2 = np.linalg.norm(uMinusX, axis=3)
    r_indices = np.argmin(l2, axis=2)

    # PLAN : create new image with only code vectors
    new_im=np.copy(image)
    H, W, RGB=new_im.shape
    for h in range(H):
        for w in range(W):
            new_im[h,w]=code_vectors[r_indices[h,w]]

    # DONOT CHANGE CODE BELOW THIS LINE
    return new_im

