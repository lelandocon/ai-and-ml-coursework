from __future__ import print_function
import numpy as np


class HMM:

    def __init__(self, pi, A, B, obs_dict, state_dict):
        """
        - pi: (1*num_state) A numpy array of initial probabilities. pi[i] = P(X_1 = s_i)
        - A: (num_state*num_state) A numpy array of transition probabilities. A[i, j] = P(X_t = s_j|X_t-1 = s_i))
        - B: (num_state*num_obs_symbol) A numpy array of observation probabilities. B[i, o] = P(Z_t = z_o| X_t = s_i)
        - obs_dict: (num_obs_symbol*1) A dictionary mapping each observation symbol to their index in B
        - state_dict: (num_state*1) A dictionary mapping each state to their index in pi and A
        """
        self.pi = pi
        self.A = A
        self.B = B
        self.obs_dict = obs_dict
        self.state_dict = state_dict


    def forward(self, Osequence):
        """
        Inputs:
        - self.pi: (1*num_state) A numpy array of initial probailities. pi[i] = P(X_1 = s_i)
        - self.A: (num_state*num_state) A numpy array of transition probailities. A[i, j] = P(X_t = s_j|X_t-1 = s_i))
        - self.B: (num_state*num_obs_symbol) A numpy array of observation probabilities. B[i, o] = P(Z_t = z_o| X_t = s_i)
        - Osequence: (1*L) A numpy array of observation sequence with length L

        Returns:
        - alpha: (num_state*L) A numpy array delta[i, t] = P(X_t = s_i, Z_1:Z_t | λ)
        """
        S = len(self.pi)
        L = len(Osequence)
        alpha = np.zeros([S, L])
        ###################################################
        # Edit here

        # PLAN: calculate the base case probabilities for each state
        # alpha for state s at beginning= (emission prob of first outcome by state s) * (initial probability of state s)
        o_index=self.obs_dict[Osequence[0]]
        alpha[:,0]=self.B[:, o_index]*self.pi

        # PLAN: calculate all other probabilities going forward
        # alpha[i,t]=(emission of o by s_i)*sum((transition from s_j to s_i)*alpha[j,t-1])
        for t in range(1, L):
            o_index=self.obs_dict[Osequence[t]]
            alpha[:, t]=self.B[:, o_index]*np.dot(self.A.T, alpha[:, t-1])
        ###################################################
        return alpha


    def backward(self, Osequence):
        """
        Inputs:
        - self.pi: (1*num_state) A numpy array of initial probailities. pi[i] = P(X_1 = s_i)
        - self.A: (num_state*num_state) A numpy array of transition probailities. A[i, j] = P(X_t = s_j|X_t-1 = s_i))
        - self.B: (num_state*num_obs_symbol) A numpy array of observation probabilities. B[i, o] = P(Z_t = z_o| X_t = s_i)
        - Osequence: (1*L) A numpy array of observation sequence with length L

        Returns:
        - beta: (num_state*L) A numpy array gamma[i, t] = P(Z_t+1:Z_T | X_t = s_i, λ)
        """
        S = len(self.pi)
        L = len(Osequence)
        beta = np.zeros([S, L])
        ###################################################
        # Edit here
        # PLAN: assign the base case probabilities for each state
        # beta for state s at the end=1
        beta[:, L-1]=1

        #PLAN: calculate all other probabilities going backwards
        # beta[i,t]=sum( (transition from s_i to s_j)*(emission of o[t] by s_i) * beta[i, t+1]
        for t in range(L-2, -1, -1):
            o_index=self.obs_dict[Osequence[t+1]]
            modified_B = self.B[:, o_index]
            modified_beta = beta[:, t + 1]
            b_and_beta=modified_beta*modified_B
            beta[:,t]= np.dot(self.A, b_and_beta)
        ###################################################
        return beta


    def sequence_prob(self, Osequence):
        """
        Inputs:
        - Osequence: (1*L) A numpy array of observation sequence with length L

        Returns:
        - prob: A float number of P(Z_1:Z_T | λ)
        """
        prob = 0
        ###################################################
        # Edit here
        # PLAN : get alpha array, then compute sum along [:,T]

        alpha=self.forward(Osequence)
        L = len(Osequence)
        prob = np.sum(alpha[:, L - 1])

        #double checking the probability against the sum of alpha*beta for all states for a given timestamp
        beta=self.backward(Osequence)
        alpha_beta=np.dot(alpha.T, beta)
        for i in range(alpha_beta.shape[0]):
            for j in range(alpha_beta.shape[1]):
                if i==j:
                    assert prob-alpha_beta[i,j]<0.1

        ###################################################
        return prob


    def posterior_prob(self, Osequence):
        """
        Inputs:
        - Osequence: (1*L) A numpy array of observation sequence with length L

        Returns:
        - prob: (num_state*L) A numpy array of P(X_t = i | O, λ)
        """
        prob = 0
        ###################################################
        # Edit here
        # PLAN : compute alpha and beta arrays
        alpha=self.forward(Osequence)
        beta=self.backward(Osequence)

        # PLAN : compute the Hadamard product of the two arrays
        prob=alpha*beta

        # PLAN : since this is a conditional probability, must divide by normalization factor
        sequence_prob=self.sequence_prob(Osequence)

        prob/=sequence_prob
        ###################################################
        return prob


    def viterbi(self, Osequence):
        """
        Inputs:
        - Osequence: (1*L) A numpy array of observation sequence with length L

        Returns:
        - path: A List of the most likely hidden state path k* (return state instead of idx)
        """
        path = []
        ###################################################
        # Edit here

        # PLAN : create a backtracking array of shape S by L
        L=len(Osequence)
        S=len(self.pi)
        delta_big=np.zeros((S,L)) # this is the backtracking array
        delta_small=np.zeros((S,L))

        # PLAN : Calculate base case probabilities
        # small delta at the beginning= (emission of first outcome by s)*(initial probability of s)
        o_index = self.obs_dict[Osequence[0]]
        delta_small[:, 0] = self.B[:, o_index] * self.pi

        # PLAN : Calculate the other probabilities going forward
        for t in range(1, L):
            o_index = self.obs_dict[Osequence[t]]
            max_product=self.A.T*delta_small[:,t-1]
            argmax_ind=np.argmax(max_product, axis=1)
            # PLAN : While calculating probabilities, also calculate entries for the backtracking array
            # delta_big[i,t] = argmax( (a from s_i to s_j)*delta_small[j, t-1])
            delta_big[:,t]=argmax_ind
            right_product=max_product[np.arange(S), argmax_ind]

            # delta_small[i,t] = b * max( (a from s_i to s_j)*delta_small[j, t-1] )
            delta_small[:,t] = self.B[:, o_index]*right_product

        # PLAN : find the state associated with the maximum delta_small at t=L
        invert_state_dict = {value: key for (key, value) in self.state_dict.items()} #I'll need this to make sense of the backtracking array
        current_state_ind=int(np.argmax(delta_small[:,L-1]))
        current_state=invert_state_dict[current_state_ind]
        path.insert(0,current_state)



        # PLAN : use the backtracking array to produce the path
        for t in range(L-1, 0, -1):
            previous_state_ind=int(delta_big[current_state_ind, t])  #I know I could reuse current_state, just useful for debugging
            previous_state=invert_state_dict[previous_state_ind]
            path.insert(0, previous_state)

            current_state_ind=previous_state_ind
            current_state=previous_state

        ###################################################
        return path
