import numpy as np
import time
import random
from hmm import HMM

'''
COLLABORATION NOTE: I collaborated with Steven Lenoski in fixing a small bug when normalizing the transition
and emission counts in the model training function
'''
def accuracy(predict_tagging, true_tagging):
	if len(predict_tagging) != len(true_tagging):
		return 0, 0, 0
	cnt = 0
	for i in range(len(predict_tagging)):
		if predict_tagging[i] == true_tagging[i]:
			cnt += 1
	total_correct = cnt
	total_words = len(predict_tagging)
	if total_words == 0:
		return 0, 0, 0
	return total_correct, total_words, total_correct*1.0/total_words


class Dataset:

	def __init__(self, tagfile, datafile, train_test_split=0.8, seed=int(time.time())):
		tags = self.read_tags(tagfile)
		data = self.read_data(datafile)
		self.tags = tags
		lines = []
		for l in data:
			new_line = self.Line(l)
			if new_line.length > 0:
				lines.append(new_line)
		if seed is not None: random.seed(seed)
		random.shuffle(lines)
		train_size = int(train_test_split * len(data))
		self.train_data = lines[:train_size]
		self.test_data = lines[train_size:]
		return

	def read_data(self, filename):
		"""Read tagged sentence data"""
		with open(filename, 'r') as f:
			sentence_lines = f.read().split("\n\n")
		return sentence_lines

	def read_tags(self, filename):
		"""Read a list of word tag classes"""
		with open(filename, 'r') as f:
			tags = f.read().split("\n")
		return tags

	class Line:
		def __init__(self, line):
			words = line.split("\n")
			self.id = words[0]
			self.words = []
			self.tags = []

			for idx in range(1, len(words)):
				pair = words[idx].split("\t")
				self.words.append(pair[0])
				self.tags.append(pair[1])
			self.length = len(self.words)
			return

		def show(self):
			print(self.id)
			print(self.length)
			print(self.words)
			print(self.tags)
			return



def model_training(train_data, tags):
	"""
	Train HMM based on training data

	Inputs:
	- train_data: (1*num_sentence) a list of sentences, each sentence is an object of line class
	- tags: (1*num_tags) a list of POS tags

	Returns:
	- model: an object of HMM class initialized with parameters(pi, A, B, obs_dict, state_dict) you calculated based on train_data
	"""
	model = None
	###################################################
	# Edit here

	# PLAN : construct the state_dict based on the tags and initialize the obs_dict
	S = len(tags)
	state_dict=dict()
	for i in range(S):
		state_dict[tags[i]]=i
	obs_dict=dict()

	# PLAN : construct the necessary containers for the counts


	transition_counts=np.zeros((S,S))
	emission_counts=list() #the list will contain S by 1 arrays that will be concatenated at the end
	pi_counts=np.zeros(S)

	# PLAN : loop over the training data, get counts for the transitions and emissions
	for sentence in train_data:
		# PLAN : update the pi counts based on the first tag of this sentence
		first_tag=sentence.tags[0]
		pi_counts[state_dict[first_tag]]+=1

		# PLAN : iterate over the words of the sentence
		for i in range(sentence.length-1):
			current_tag=sentence.tags[i]
			next_tag=sentence.tags[i+1]
			current_word=sentence.words[i]

			# PLAN : count the transition from current state to next state
			transition_counts[state_dict[current_tag], state_dict[next_tag]]+=1

			#PLAN : count the emission of current word from current state
			increment_emissions_counts(emission_counts, obs_dict, state_dict, current_word, current_tag)

		# PLAN : since we need to count the last emission but not the last transition (from a state to 'END'),
		# we will count the last emission here in a loop-and-a-half structure
		last_word=sentence.words[sentence.length-1]
		last_tag=sentence.tags[sentence.length-1]
		increment_emissions_counts(emission_counts, obs_dict, state_dict, last_word, last_tag)

	# PLAN : concatenate emissions_counts into an array
	emission_counts=np.concatenate(emission_counts, axis=1)

	# PLAN : normalize the counts of the transitions and the emissions, thus forming A and B
	trans_factor=np.sum(transition_counts, axis=1)
	transition_counts=transition_counts/np.reshape(trans_factor, (transition_counts.shape[0], 1))
	transition_counts[np.isnan(transition_counts)]=0

	em_factor = np.sum(emission_counts, axis=1)
	emission_counts = emission_counts / np.reshape(em_factor, (emission_counts.shape[0], 1))
	emission_counts[np.isnan(emission_counts)]=0

	pi_counts=pi_counts/np.sum(pi_counts)

	# PLAN : create an instance of a hidden Markov model class and assign it to the model variable
	model=HMM(pi_counts, transition_counts, emission_counts, obs_dict, state_dict)

	###################################################
	return model


def increment_emissions_counts(emission_counts, obs_dict, state_dict, current_word, current_tag):
	'''
	helper function for model training that modifies the emission counts and obs_dict
	according to the current_tag and current state
	:param emission_counts: a list which contains a S by 1 array for every word encountered
	:param obs_dict: dictionary where keys are current_tags and values are their indices in emission_counts
	:param state_dict: dictionary where keys are current_states and values are their indices in emission_counts
	:param current_word: the word currently being inspected (type: string)
	:param current_tag: the tag currently being inspected (type: string)
	:return: nothing
	'''
	# PLAN : check to see if the word is in the obs_dict and has a column in emission_counts
	S=len(state_dict)
	try:
		current_word_ind = obs_dict[current_word]
	except KeyError:
		L = len(obs_dict)
		obs_dict[current_word] = L
		current_word_ind = L
		emission_counts_array=np.zeros((S,1))
		emission_counts.append(emission_counts_array)

	# PLAN : count the emission of word by the current state
	emission_counts[current_word_ind][state_dict[current_tag]]+= 1

	return


def speech_tagging(test_data, model, tags):
	"""
	Inputs:
	- test_data: (1*num_sentence) a list of sentences, each sentence is an object of line class
	- model: an object of HMM class

	Returns:
	- tagging: (num_sentence*num_tagging) a 2D list of output tagging for each sentences on test_data
	"""
	tagging = []
	###################################################
	# Edit here

	S=len(model.pi)
	# PLAN : iterate over the test data
	new_word_emission_prob=10**(-6)
	new_word_emission=np.full((S,1), new_word_emission_prob)
	for sentence in test_data:
		# PLAN : check for new words/observations. If they are new, add them to obs_dict and change the emission array
		for word in sentence.words:
			try:
				o_index=model.obs_dict[word]
			except KeyError:
				L = len(model.obs_dict)
				model.obs_dict[word] = L
				current_word_ind = L
				model.B=np.append(model.B, new_word_emission, axis=1)

		# PLAN : run Viterbi function over a given sentence and append to results to tagging
		predicted_tags=model.viterbi(sentence.words)
		tagging.append(predicted_tags)

	###################################################
	return tagging

