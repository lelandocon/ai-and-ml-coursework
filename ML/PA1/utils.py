import numpy as np
from typing import List
from hw1_knn import KNN


def Information_Gain(S, branches):
    # S: float
    # branches: List[List[int]] num_branches * num_cls
    # return: float

    ''' branches can be intrepreted in a few ways.
        First, it can be seen as a list of occurrences, where the inner list is the occurrences of labels for a given branch.
        Second, it can be seen as a list of counts, where the inner list contains counts of the labels for a given branch.
        I will implement the second interpretation.'''

    newEntropy=0.0
    totalExamplesAmongBranches=0
    # PSEUDO/PLAN: find the total number of examples in all branches
    for branch in branches:
        totalExamplesAmongBranches+=np.sum(branch)
    # PSEUDO/PLAN: calculate the conditional entropy of each branch
    for branch in branches:
        examplesWithinBranch=np.sum(branch)
        conditionalEntropyForBranch=calculateEntropy(branch)*examplesWithinBranch/float(totalExamplesAmongBranches)
        newEntropy+=conditionalEntropyForBranch
    # PSEUDO/PLAN: subtract the new entropy from the old entropy (given by S)
    infoGain=S-newEntropy
    return infoGain

def calculateEntropy(labelCounts):
    entropy=0
    totalExamples=np.sum(labelCounts)
    for labelCount in labelCounts:
        labelProbability=labelCount/float(totalExamples)
        if(labelProbability>0):
            entropy-=labelProbability*np.log2(labelProbability)
    return entropy





# TODO: implement reduced error prunning function, pruning your tree on this function
def reduced_error_prunning(decisionTree, X_test, y_test):
    # decisionTree
    # X_test: List[List[any]]
    # y_test: List
    #PSEUDO/PLAN: This will be a wrapper function for reduced_error_pruning_node
    reduced_error_pruning_node(decisionTree, X_test, y_test, decisionTree.root_node)
    return

def reduced_error_pruning_node(decisionTree, X_test, y_test, currentNode):
    # PSEUDO/PLAN: traverse the tree by calling function recursively on children
    for child in currentNode.children:
        reduced_error_pruning_node(decisionTree, X_test, y_test, child)

    if(len(currentNode.children)!=0): #slight timesaver, since you can't actually prune leaf nodes, though it's mechanically allowed
        oldChildList=currentNode.children
        pred_y_test=decisionTree.predict(X_test)
        oldAccuracy=myOwnAccuracyScore(y_test, pred_y_test)

        # PSEUDO/PLAN: try to prune at this node i.e. replace the children list with an empty list
        # and see if the prediction accuracy improves
        currentNode.children=[]
        new_pred_y_test=decisionTree.predict(X_test)
        newAccuracy=myOwnAccuracyScore(y_test, new_pred_y_test)

        # PSEUDO/PLAN: if the accuracy does improve, then keep the empty list in place. If not, then reinsert the old list.
        if newAccuracy<=oldAccuracy:
            currentNode.children=oldChildList
    return

def myOwnAccuracyScore(realLabels, predictedLabels):
    numCorrectPredictions=0
    numTotalPredictions=0.0
    for realLabelIndex in range(len(realLabels)):
        if realLabels[realLabelIndex]==predictedLabels[realLabelIndex]:
            numCorrectPredictions+=1
        numTotalPredictions+=1
    return numCorrectPredictions/numTotalPredictions




# print current tree
def print_tree(decisionTree, node=None, name='branch 0', indent='', deep=0):
    if node is None:
        node = decisionTree.root_node
    print(name + '{')

    print(indent + '\tdeep: ' + str(deep))
    string = ''
    label_uniq = np.unique(node.labels).tolist()
    for label in label_uniq:
        string += str(node.labels.count(label)) + ' : '
    print(indent + '\tnum of samples for each class: ' + string[:-2])

    if node.splittable:
        print(indent + '\tsplit by dim {:d}'.format(node.dim_split))
        for idx_child, child in enumerate(node.children):
            print_tree(decisionTree, node=child, name='\t' + name + '->' + str(idx_child), indent=indent + '\t', deep=deep+1)
    else:
        print(indent + '\tclass:', node.cls_max)
    print(indent + '}')



def f1_score(real_labels: List[int], predicted_labels: List[int]) -> float:
    assert len(real_labels) == len(predicted_labels)
    numerator=0
    for labelIndex in range(len(real_labels)):
        numerator+=real_labels[labelIndex]*predicted_labels[labelIndex]
    numerator*=2

    denominator=np.sum(real_labels)+np.sum(predicted_labels)
    return numerator/float(denominator)


def euclidean_distance(point1: List[float], point2: List[float]) -> float:
    vectorBetween=np.array(point1)-np.array(point2)
    return np.sqrt(inner_product_distance(vectorBetween,vectorBetween))



def inner_product_distance(point1: List[float], point2: List[float]) -> float:
    '''Since the values we will be using are in a real n-space, the inner product
    becomes a dot product. Thus, I will implement the dot product in this function'''
    innerProductSum=0
    for pointIndex in range(len(point1)):
        innerProductSum+=point1[pointIndex]*point2[pointIndex]
    return innerProductSum



def gaussian_kernel_distance(point1: List[float], point2: List[float]) -> float:
    vectorBetween = np.array(point1) - np.array(point2)
    return -np.exp(-inner_product_distance(vectorBetween,vectorBetween)*1.0/2.0)



def cosine_sim_distance(point1: List[float], point2: List[float]) -> float:
    dotProduct=inner_product_distance(point1, point2) #may need to change?
    zeroVector=np.zeros(len(point1))
    cosine_similarity=dotProduct / float(euclidean_distance(point1, zeroVector)) / float(euclidean_distance(point2, zeroVector))
    cosine_distance=1-cosine_similarity
    return cosine_distance


# TODO: select an instance of KNN with the best f1 score on validation dataset
def model_selection_without_normalization(distance_funcs, Xtrain, ytrain, Xval, yval):
    # distance_funcs: dictionary of distance funtion
    # Xtrain: List[List[int]] train set
    # ytrain: List[int] train labels
    # Xval: List[List[int]] validation set
    # yval: List[int] validation labels
    # return best_model: an instance of KNN
    # return best_k: best k choosed for best_model
    # return best_func: best function choosed for best_model
    MAX_K=30
    best_model=None
    best_k=MAX_K+1 #dummy value for tiebreak
    best_func_key="cosine_dist" #dummy value for tiebreak
    maxF1=0
    distance_func_tiebreak = {
        'euclidean': 4,
        'gaussian': 3,
        'inner_prod': 2,
        'cosine_dist': 1,
    }
    # PSEUDO/PLAN: iterate over a range of k values
    if(len(Xtrain)<=MAX_K): #make sure you're not looking for more neighbors than there are data points
        MAX_K=len(Xtrain)-1
    assert(len(Xtrain)>MAX_K)
    for k in range(1,MAX_K,2):
        for distance_func_key in distance_funcs.keys():
            changeMax=False
            distance_func=distance_funcs[distance_func_key]
            # PSEUDO/PLAN: create KNN for a given distance function and k value
            newKNN=KNN(k, distance_func)
            newKNN.train(Xtrain, ytrain) # PSEUDO/PLAN: train the KNN i.e. associate a data set with it
            predictedLabels=newKNN.predict(Xval)  # PSEUDO/PLAN: make predictions on the validation set
            f1Score=f1_score(yval, predictedLabels) # PSEUDO/PLAN: calculate F1 score
            # PSEUDO/PLAN: compare F1 scores, with higher being better
            if f1Score>maxF1:
                changeMax=True
            elif f1Score==maxF1:
                # PSEUDO/PLAN: use tiebreaks based on distance function (Euclidean>Gaussian>InnerProduct>Cosine)
                if distance_func_tiebreak[distance_func_key]>distance_func_tiebreak[best_func_key]:
                    changeMax=True
                elif distance_func_tiebreak[distance_func_key]==distance_func_tiebreak[best_func_key]:
                    # PSEUDO/PLAN: use tiebreaks based on k, where the lesser k is preferred
                    if k<best_k:
                        changeMax=True

            if(changeMax):
                best_k=k #technically unnecessary, since it is encoded in bestModel
                best_func_key=distance_func_key
                maxF1=f1Score
                best_model=newKNN
    # PSEUDO/PLAN: return the model, the k number, and the distance function that produced the highest F1
    print("Highest F1 Score: "+str(maxF1))
    return (best_model, best_k, best_func_key)



# TODO: select an instance of KNN with the best f1 score on validation dataset, with normalized data
def model_selection_with_transformation(distance_funcs, scaling_classes, Xtrain, ytrain, Xval, yval):
    # distance_funcs: dictionary of distance funtion
    # scaling_classes: diction of scalers
    # Xtrain: List[List[int]] train set
    # ytrain: List[int] train labels
    # Xval: List[List[int]] validation set
    # yval: List[int] validation labels
    # return best_model: an instance of KNN
    # return best_k: best k choosed for best_model
    # return best_func: best function choosed for best_model
    # return best_scaler: best function choosed for best_model

    best_model = None
    best_k = 0
    best_func_key = None
    maxF1 = 0

    scaling_classes = {
        'min_max_scale': MinMaxScaler,
        'normalize': NormalizationScaler,
    }
    for ScalerClassKey in scaling_classes.keys():

        changeMax=False
        scaler=scaling_classes[ScalerClassKey]()
        # PSEUDO/PLAN: transform the Xtrain and Xval data
        scaledXtrain, scaledXval=scaler(Xtrain), scaler(Xval)
        # PSEUDO/PLAN: perform model selection
        scaledModel, scaledK, scaledDistKey=model_selection_without_normalization(distance_funcs, scaledXtrain, ytrain, scaledXval, yval)
        # PSEUDO/PLAN: unfortunately, since F1 score is not returned, we must repeat the predictions
        scaledPredictedLabels=scaledModel.predict(scaledXval)
        f1Score=f1_score(yval, scaledPredictedLabels)
        # PSEUDO/PLAN: compare results/initiate tiebreaking protocol
        if f1Score>maxF1:
            changeMax=True
        elif f1Score==maxF1 and scaling_classes[ScalerClassKey]==MinMaxScaler:
            changeMax=True

        if(changeMax):
            best_k = scaledK  # technically unnecessary, since it is encoded in bestModel
            best_func_key = scaledDistKey
            maxF1 = f1Score
            best_model = scaledModel
            best_scaler_key = ScalerClassKey

    # PSEUDO/PLAN: return the best model
    return (best_model, best_k, best_func_key, best_scaler_key)


class NormalizationScaler:
    def __init__(self):
        pass

    #TODO: normalize data
    def __call__(self, features: List[List[float]]) -> List[List[float]]:
        """
        normalize the feature vector for each sample . For example,
        if the input features = [[3, 4], [1, -1], [0, 0]],
        the output should be [[0.6, 0.8], [0.707107, -0.707107], [0, 0]]
        """
        # PSEUDO/PLAN: iterate over vectors/examples
        origin=np.zeros(len(features[0]))
        normalizedFeatures=[]
        for vector in features:
            # PSEUDO/PLAN: get the magnitude of vector
            vectorMag=euclidean_distance(vector, origin)
            # PSEUDO/PLAN: create new vector by dividing old vector by its magnitude
            if(int(vectorMag)==0):
                newVector=vector
            else:
                newVector=np.array(vector)/float(vectorMag)
            normalizedFeatures.append(newVector)
        return normalizedFeatures


class MinMaxScaler:
    """
    You should keep some states inside the object.
    You can assume that the parameter of the first __call__
        must be the training set.

    Hints:
        1. Use a variable to check for first __call__ and only compute
            and store min/max in that case.

    Note:
        1. You may assume the parameters are valid when __call__
            is being called the first time (you can find min and max).

    Example:
        train_features = [[0, 10], [2, 0]]
        test_features = [[20, 1]]

        scaler = MinMaxScale()
        train_features_scaled = scaler(train_features)
        # now train_features_scaled should be [[0, 1], [1, 0]]

        test_features_sacled = scaler(test_features)
        # now test_features_scaled should be [[10, 0.1]]

        new_scaler = MinMaxScale() # creating a new scaler
        _ = new_scaler([[1, 1], [0, 0]]) # new trainfeatures
        test_features_scaled = new_scaler(test_features)
        # now test_features_scaled should be [[20, 1]]
    """
    def __init__(self):
        pass

    def __call__(self, features: List[List[float]]) -> List[List[float]]:
        """
        normalize the feature vector for each sample . For example,
        if the input features = [[2, -1], [-1, 5], [0, 0]],
        the output should be [[1, 0], [0, 1], [0.333333, 0.16667]]
        """
        minsAndMaxes={}
        minMaxFeatures=[]
        min_key, max_key = "min", "max"
        try:
            # check to see if self.mins_and_maxes exist
            # i.e. if training data has already been fed into the scaler
            minsAndMaxes=self.mins_and_maxes
        except AttributeError: #if not, then create the min-max ranges
            # PSEUDO/PLAN: loop-and-a-half: take first example so as to establish first values for mins and maxes
            firstExample=features[0]
            for attributeIndex in range(len(firstExample)):
                attributeValue=firstExample[attributeIndex]
                minsAndMaxes[attributeIndex]={min_key:attributeValue, max_key:attributeValue}
            # PSEUDO/PLAN: iterate over examples
            for example in features:
                # PSEUDO/PLAN: iterate over the attributes of a given example
                for attributeIndex in range(len(example)):
                    attributeValue=example[attributeIndex]
                    # PSEUDO/PLAN: check to see if the value of that attribute is a max or min
                    if attributeValue > minsAndMaxes[attributeIndex][max_key]:
                        minsAndMaxes[attributeIndex][max_key]=attributeValue
                    if attributeValue < minsAndMaxes[attributeIndex][min_key]:
                        minsAndMaxes[attributeIndex][min_key] = attributeValue
                        self.mins_and_maxes=minsAndMaxes
        # PSEUDO/PLAN: transform each vector/example
        for example in features:
            minMaxExample=[]
            for attributeIndex in range(len(example)):
                oldValue=example[attributeIndex]
                minValue, maxValue=minsAndMaxes[attributeIndex][min_key], minsAndMaxes[attributeIndex][max_key]
                if(minValue==maxValue):
                    newValue=0
                else:
                    newValue=(oldValue-minValue)/float(maxValue-minValue)
                minMaxExample.append(newValue)
            minMaxFeatures.append(minMaxExample)
        return minMaxFeatures





