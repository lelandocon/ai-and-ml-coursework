from __future__ import division, print_function

from typing import List

import numpy as np
import scipy

############################################################################
# DO NOT MODIFY ABOVE CODES
############################################################################

class KNN:

    def __init__(self, k: int, distance_function):
        self.k = k
        self.distance_function = distance_function

    def train(self, features: List[List[float]], labels: List[int]):
        # features: List[List[float]] a list of points
        # labels: List[int] labels of features
        self.features=features
        self.labels=labels


    def predict(self, features: List[List[float]]) -> List[int]:
        # features: List[List[float]] a list of points
        # return: List[int] a list of predicted labels
        predictedLabels=[]
        for example in features:
            # PSEUDO/PLAN: find k-nearest neighbors
            nearestNeighborsLabels=self.get_k_neighbors(example)
            # PSEUDO/PLAN: get the majority label
            predictedLabel=np.bincount(nearestNeighborsLabels).argmax()
            predictedLabels.append(predictedLabel)
        return predictedLabels


    def get_k_neighbors(self, point: List[float]) -> List[int]:
        # point: List[float] one example
        # return: List[int] labels of K nearest neighbor
        # PSEUDO/PLAN: go through examples
        # PSEUDO/PLAN: calculate distance
        # PSEUDO/PLAN: compare to list/insert in list
        labelsAndDistances=[]
        for exampleIndex in range(len(self.features)):
            example=self.features[exampleIndex]
            distanceFromExample=self.distance_function(point, example)
            labelsAndDistances.append((self.labels[exampleIndex], distanceFromExample))
        labelsAndDistances.sort(key=lambda labelAndDistance: labelAndDistance[1])
        assert(len(labelsAndDistances)==len(self.labels))
        assert(self.k<=len(labelsAndDistances))
        nearestNeighborsLabels=[labelsAndDistances[i][0] for i in range(self.k)]
        return nearestNeighborsLabels

        '''
        for example in self.features:
            distanceFromPoint=self.distance_function(example, point)
            if(len(distances)<self.k):
                distances.append(distanceFromPoint)
                distances.sort()
            else:
                i=-1
                while(distanceFromPoint<distances[i]):
                    if(i=-1):
                        distances[i]=distanceFromPoint
                    else:
                        oldDistance=distances[i]
                        distance[i]=distanceFromPoint
                        distances[i+1]=oldDistance
                    i-=1
        return distances
        '''





if __name__ == '__main__':
    print(np.__version__)
    print(scipy.__version__)
