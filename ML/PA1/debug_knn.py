import numpy as np
from hw1_knn import KNN
from utils import euclidean_distance, gaussian_kernel_distance, inner_product_distance, cosine_sim_distance
from utils import f1_score, model_selection_without_normalization, model_selection_with_transformation
distance_funcs = {
    'euclidean': euclidean_distance,
    'gaussian': gaussian_kernel_distance,
    'inner_prod': inner_product_distance,
    'cosine_dist': cosine_sim_distance,
}


from data import data_processing
Xtrain, ytrain, Xval, yval, Xtest, ytest = data_processing()

'''
best_model, best_k, best_function = model_selection_without_normalization(distance_funcs, Xtrain, ytrain, Xval, yval)
print("Pre-transformation:\n")
print("Best model: "+str(best_model))
print("Best_k: "+str(best_k))
print("Best_function: "+str(best_function))
'''

from utils import NormalizationScaler, MinMaxScaler

scaling_classes = {
    'min_max_scale': MinMaxScaler,
    'normalize': NormalizationScaler,
}

best_model, best_k, best_function, best_scaler = model_selection_with_transformation(distance_funcs, scaling_classes, Xtrain, ytrain, Xval, yval)
print("Post-transformation:\n")
print("Best model: "+str(best_model))
print("Best_k: "+str(best_k))
print("Best_function: "+str(best_function))