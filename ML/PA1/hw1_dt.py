import numpy as np
import utils as Util


class DecisionTree():
    def __init__(self):
        self.clf_name = "DecisionTree"
        self.root_node = None

    def train(self, features, labels):
        # features: List[List[float]], labels: List[int]
        # init
        assert (len(features) > 0)
        self.feature_dim = len(features[0])
        num_cls = np.unique(labels).size

        # build the tree
        self.root_node = TreeNode(features, labels, num_cls)
        if self.root_node.splittable:
            self.root_node.split()

        return

    def predict(self, features):
        # features: List[List[any]]
        # return List[int]
        y_pred = []
        for idx, feature in enumerate(features):
            pred = self.root_node.predict(feature)
            y_pred.append(pred)
            # print ("feature: ", feature)
            # print ("pred: ", pred)
        return y_pred


class TreeNode(object):
    def __init__(self, features, labels, num_cls):
        # features: List[List[any]], labels: List[int], num_cls: int
        self.features = features
        self.labels = labels
        self.children = []
        self.num_cls = num_cls
        # find the most common labels in current node
        count_max = 0
        for label in np.unique(labels):
            if self.labels.count(label) > count_max:
                count_max = labels.count(label)
                self.cls_max = label
                # splitable is false when all features belongs to one class
        if len(np.unique(labels)) < 2:
            self.splittable = False
        else:
            self.splittable = True

        self.dim_split = None  # the index of the feature to be split

        self.feature_uniq_split = None  # the possible unique values of the feature to be split

    #QUESTION: where is the possible unique values for an attribute found universally? i.e. outside of a given node?
    def split(self):
        if(self.splittable): #BASE CASE: all examples are of one class
            FEATURES="features" #key for the nested hash map
            LABELS="labels"
            # PSEUDO/PLAN: calculate entropy at this node
            uniqueLabels, labelCounts=np.unique(self.labels, return_counts=True)
            oldEntropy=Util.calculateEntropy(labelCounts)
            maxInfoGain=0
            # BASE CASE: no examples left (this code should never be reached since self.spittable would be false)
            if(len(self.features)==0):
                return
            # PSEUDO/PLAN: decide which feature to split on by information gain
            for splitIndex in range(len(self.features[0])):
                #will use a nested hash map
                #first level will have feature value as the key and a hash map representing the relevant branch as the value
                #Revision: second level has the "features" and "labels" as keys and the associated lists as values
                featureValues=[] #TODO : refactor this section into helper function
                bundleOfBranches={}
                for featureIndex in range(len(self.features)):
                    feature=self.features[featureIndex]
                    featureValue=feature[splitIndex]
                    label=self.labels[featureIndex]
                    #navigate the first level
                    if(featureValue in bundleOfBranches):
                        branch=bundleOfBranches[featureValue]
                    else:
                        branch={}
                        bundleOfBranches[featureValue]=branch
                    #navigate the second level
                    if(FEATURES in branch):
                        branch[FEATURES].append(feature)
                        branch[LABELS].append(label)
                    else:
                        branch[FEATURES]=[feature]
                        branch[LABELS]=[label]
                #assemble the counts of labels needed for "branches"
                branches=[]
                for branchKey in bundleOfBranches.keys():
                    uniqueLabels, labelCounts=np.unique(bundleOfBranches[branchKey][LABELS], return_counts=True)
                    branches.append(labelCounts)
                #calculate info gain
                infoGain=Util.Information_Gain(oldEntropy, branches)
                changeInfoGain=False
                if infoGain>maxInfoGain:
                    changeInfoGain=True
                elif infoGain==maxInfoGain and infoGain>0:
                    if len(bundleOfBranches.keys())>len(self.feature_uniq_split):
                        changeInfoGain=True
                    elif len(bundleOfBranches.keys())==len(self.feature_uniq_split):
                        if(splitIndex<self.dim_split):
                            changeInfoGain=True
                if(changeInfoGain):
                    maxInfoGain=infoGain
                    maxBundle=bundleOfBranches
                    # PSEUDO/PLAN: store the index of the feature to split as dim_split and the unique values as feature_uniq_split
                    self.dim_split=splitIndex
                    self.feature_uniq_split=bundleOfBranches.keys()
            # PSEUDO/PLAN: BASE CASE: if the max information gain is zero, then further splitting will not help
            # (possibly because all features have already been split in this path, but maybe not)
            if(maxInfoGain==0):
                self.splittable=False
                return
            # PSEUDO/PLAN: create child nodes based on the different values of the feature
            for branchKey in sorted(maxBundle.keys()):
                branch=maxBundle[branchKey]
                childNode=TreeNode(branch[FEATURES], branch[LABELS], len(np.unique(branch[LABELS])))
                # RECURSIVE CASE ???????????????????????????????????????????????????????????????????
                childNode.split()
                # PSEUDO/PLAN: store pointer to child in list of children
                self.children.append(childNode)


    # QUESTION: Predict the branch? Shouldn't this just return a class?
    def predict(self, feature):
        # feature: List[any]
        # return: int
        if(self.dim_split!=None):
            featureValue=feature[self.dim_split]
            # PSEUDO/PLAN: examine the children of the current node
            for child in self.children:
                if(len(child.features)>0):
                    # PSEUDO/PLAN: for each child, compare the attribute value of the given example to the attribute value
                    # of an example from that child. If there is a match, further explore that branch.
                    sampleChildValue=child.features[0][self.dim_split]
                    if featureValue==sampleChildValue:
                        # PSEUDO/PLAN: RECURSIVE CASE: use predict on the child node
                        return child.predict(feature)

        # PSEUDO/PLAN: BASE CASE: if there are no such matches from the children, then simply take the majority class
        #  from this current node, since this example has nowhere else to go in the tree
        # PSEUDO/PLAN: BASE CASE: this node has no children i.e. it is a leaf node. Return majority class of this node.
        return self.cls_max


        '''A note: given the construction of the tree and how each branch has at least one example in order
        for it to be grafted onto the tree, we don't need a base case for leaves not having any examples.'''
        # PSEUDO/PLAN: BASE CASE 2: a child does not have any examples. Takes majority class of parent's examples.
