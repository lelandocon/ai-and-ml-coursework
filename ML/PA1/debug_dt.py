import data
import hw1_dt as decision_tree
import utils as Utils
from sklearn.metrics import accuracy_score

'''
features, labels = data.sample_decision_tree_data()


# build the tree
dTree = decision_tree.DecisionTree()
dTree.train(features, labels)

# print
Utils.print_tree(dTree)

# data
X_test, y_test = data.sample_decision_tree_test()

# testing
y_est_test = dTree.predict(X_test)
test_accu = accuracy_score(y_est_test, y_test)
print('test_accu', test_accu)
'''



#load data
X_train, y_train, X_test, y_test = data.load_decision_tree_data()

# set classifier
dTree = decision_tree.DecisionTree()

# training
dTree.train(X_train.tolist(), y_train.tolist())

#Utils.print_tree(dTree)

y_est_test = dTree.predict(X_test)
test_accu = accuracy_score(y_est_test, y_test)
print('test_accu', test_accu)

Utils.reduced_error_prunning(dTree, X_test, y_test)

y_est_test = dTree.predict(X_test)
test_accu = accuracy_score(y_est_test, y_test)
print('test_accu', test_accu)

Utils.print_tree(dTree)