# Introduction
This repository includes the graduate-level coursework for artificial intelligence and machine learning, as taken at USC in 2018/2019.


# Where my contributions lie
The AI assignments were started from scratch and thus consist largely of my own work/code.

The ML assignments had frameworks written by TAs, so to find my contributions, look to the core functions, especially those with the PSEUDO/PLANNING comments.

# If you only have time to look at one part of this repository to get a sense of my work...

...then look at AI, HW1b. I give the problem description and my reasoning in detail, and, as indicated above, I wrote that solution from scratch.


# Summaries of assignments
Here are brief summaries of each assignment:

*  AI
    *  HW1b : solve an n-queens-like problem with search
    *  HW2 : secure resources in midst of adversarial agent with game-playing
	*  HW3 : create policy for navigating a grid with rewards and penalties with Markov Decision Process
*  ML
	* PA1 : implement k-nearest neighbors and decision tree algorithms
	* PA2 : implement linear/polynomial regression as well as binary and multiclass logistic and perceptron losses
	* PA3 : implement fundamental elements of a neural network
	* PA4 : implement k-means clustering and Hidden Markov Model algorithms